from django.forms import CharField, TextInput, ModelMultipleChoiceField, SelectMultiple, ModelChoiceField, Select


class CustomPhoneField(CharField):

    def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.required = False
            self.max_length = 10
            self.widget = TextInput(attrs={'type': "tel",
                                           'id': "example-tel-input",
                                           'pattern': "\d{10}",
                                           'placeholder': 'formato: 8092221111 o vacío',
                                           'maxlength': '10'
                                           })


class CustomEmailField(CharField):

    def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.required = False
            self.max_length = 10
            self.widget = TextInput(attrs={'type': "email",
                                           'id': "example-email-input",
                                           'pattern': "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$",
                                           'placeholder': 'formato: ejemplo@example.com'
                                           })


class CustomMultipleChoice(ModelMultipleChoiceField):

    def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.queryset = kwargs.get('queryset')
            self.required = kwargs.get('required')
            self.widget = SelectMultiple()
            if kwargs.get('required') is None:
                self.required = False


class CustomSingleChoice(ModelChoiceField):

    def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.queryset = kwargs.get('queryset')
            self.required = kwargs.get('required')
            self.widget = Select()
            if kwargs.get('required') is None:
                self.required = False
