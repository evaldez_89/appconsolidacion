from collections import namedtuple

from django.db.models import QuerySet, Q, Manager

from consolidacion.enumerables import CreyenteStatus


class CreyenteQuerySet(QuerySet):
    def by_consolidador(self, consolidador_id: int):
        # el operado "|" suma dos query sets
        return (self.filter(consolidador_id=consolidador_id)).order_by('nombre')

    def by_lider(self, consolidadores_list_ids):
        """consolidadores_list_ids = all the ids of the consolidadores assigned to one lider
        including the consolidador id of lider  himself"""
        return (self.filter(consolidador_id__in=consolidadores_list_ids)).order_by('nombre')

    def lider_network(self, lider):
        """lider = the list of creyentes assign to a lider or its disciples"""
        if type(lider) == int:
            lider_q = LeaderQuerySet()
            lider = lider_q.filter(id=lider).first()
            if not lider:
                return self.none()
        return (self.filter(consolidador__leadaer_id=lider.id) |
                self.filter(consolidador__nombre=lider.nombre)).order_by('nombre')

    def by_status(self, status_of_creyente: str):
        StatusList = namedtuple('StatusList', ['status', 'lista'])
        aviso = "Mostrando Creyentes"
        lista = None

        if status_of_creyente == "siguiendo":
            lista = StatusList(f"{aviso} En Seguimiento",
                               self.exclude(status__in=[CreyenteStatus.DELETED, CreyenteStatus.PROMOTED, CreyenteStatus.CONSOLIDATED]))
        elif status_of_creyente == "consolidado":
            lista = StatusList(f"{aviso} Consolidados", self.filter(status=CreyenteStatus.CONSOLIDATED))
        elif status_of_creyente == "baja":
            lista = StatusList(f"{aviso} De Baja", self.filter(CreyenteStatus.DELETED))
        elif status_of_creyente == "todo":
            lista = StatusList(f"{aviso} Todos Los Creyentes", self.all())

        return lista


class ConsolidadorManager(Manager):
    def get_queryset(self):
        return super(ConsolidadorManager, self).get_queryset().filter(is_deleted=False).filter(is_active=True)

    def find_by_phone_number(self, phone_number):
        return self.filter(
            Q(telefono=phone_number) |
            Q(cel=phone_number) |
            Q(otro=phone_number)
        )


class LeaderQuerySet(QuerySet):
    def all_with_email(self):
        return self.exclude(email__isnull=True).exclude(email__exact='')
