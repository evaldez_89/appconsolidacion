from django import template
from time import strptime, strftime
from datetime import datetime
from consolidacion import models
from Funciones import get_hoy, user_has_group, user_has_access
from django.contrib.auth.models import User

from consolidacion.enumerables import Access

register = template.Library()


@register.filter()
def capitaliza(p):
    if len(p) > 0:
        return p.capitalize()


@register.filter()
def plural(p, s):
    if len(p) > 0:
        return p + s


@register.filter()
def totalCreyentes(idc, celula=False):
    # devuelve el total de creyentes asignados a un consolidador
    total = 0

    if not celula:
        # total de creyente sin consolidador secundario y con el primario siendo el actual
        como_primario = models.Creyente.objects.all().filter(consolidador=int(idc))

        # sumar las listas
        total += como_primario.filter(celula=None).count()
    else:
        total += models.Creyente.objects.filter(celula_id=idc).count()

    return total


@register.filter()
def totalConsolidadores(id_lider: int):
    # devuelve el total de consolidadores asignados a un lider
    return models.Consolidador.active_objects.all().filter(leader_id=id_lider).count()


@register.filter()
def calc_dia(fecha):
    # devuelve dia de la semana
    n_dia = fecha.weekday()
    dia = ""
    if n_dia == 0:
        dia = "Lunes"
    if n_dia == 1:
        dia = "Martes"
    if n_dia == 2:
        dia = "Miércoles"
    if n_dia == 3:
        dia = "Jueves"
    if n_dia == 4:
        dia = "Viernes"
    if n_dia == 5:
        dia = "Sábado"
    if n_dia == 6:
        dia = "Domingo"
    return dia + " " + fecha.strftime("%d/%m/%Y")


@register.filter(name='has_group')
def has_group(user, group_name):
    return user_has_group(user, group_name)


@register.filter()
def getLiderId(name):
    lider_id = models.Consolidador.active_objects.filter(nombre=name, es_lider=True).first()
    return lider_id.id if lider_id else None


@register.filter()
def fecha(days):
    return datetime.strptime(get_hoy(days), "%d/%m/%Y").strftime("%Y-%m-%d")


@register.filter()
def modelo(obj) -> str:
    return obj.__class__.__name__


def class_name(obj) -> str:
    return obj.__name__


@register.filter()
def is_user(email):
    return User.objects.filter(email=email).first() is not None


@register.filter()
def is_consolidador(name):
    return models.Consolidador.active_objects.filter(nombre=name).first() is not None


@register.filter()
def is_leader(name):
    return models.Consolidador.active_objects.filter(nombre=name, es_lider=True).first() is not None


@register.filter()
def check_access_level(user: User, level_names: str):
    try:
        levels = [Access[level_name.upper()] for level_name in level_names.split("-")]
        return user_has_access(user, *[access for access in levels])
    except ValueError:
        return False


@register.filter()
def format_cellphone_number(cellphone: str):
    return cellphone if cellphone.startswith("1") else f"1{cellphone}"
