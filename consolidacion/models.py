#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import timedelta, datetime

from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField

from Funciones import calc_etapa
from consolidacion import managers
from os import linesep

from consolidacion.enumerables import CreyenteStatus


class Etapa(models.Model):
    etapa = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.etapa


class Dia(models.Model):
    dia = models.CharField(max_length=12, unique=True)

    def __str__(self):
        return self.dia


class Frecuencia(models.Model):
    frecuencia = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.frecuencia


class Razon(models.Model):
    # razon para dar de baja a un creyente.
    razon = models.CharField(max_length=25, unique=True)

    class Meta:
        verbose_name_plural = "Razones"

    def __str__(self):
        return self.razon


class Actividad(models.Model):
    actividad = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.actividad

    class Meta:
        verbose_name_plural = "Actividades"

    def get_absolute_url(self):
        # return reverse('actualizar-actividad', kwargs={'pk': self.pk})
        return reverse('agregar-actividad')


class Laboral(models.Model):
    estado_laboral = models.CharField(max_length=20, unique=True)

    class Meta:
        verbose_name_plural = "Estados Laborales"

    def __str__(self):
        return self.estado_laboral


class Ocupacion(models.Model):
    ocupacion = models.CharField(max_length=20, unique=True)

    class Meta:
        verbose_name_plural = "Ocupaciones"

    def __str__(self):
        return self.ocupacion

    def get_absolute_url(self):
        # return reverse('actualizar-ocupacion', kwargs={'pk': self.pk})
        return reverse('agregar-ocupacion')


class Civil(models.Model):
    estado_civil = models.CharField(max_length=20, unique=True)

    class Meta:
        verbose_name_plural = "Estados Civiles"

    def __str__(self):
        return self.estado_civil


class Sexo(models.Model):
    sexo = models.CharField(max_length=20, unique=True)

    class Meta:
        verbose_name_plural = "Sexos (M/F)"

    def __str__(self):
        return self.sexo


class Trayectoria(models.Model):
    proceso = models.CharField(max_length=20, unique=True)
    completado = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return self.proceso


class Consolidador(models.Model):
    nombre = models.CharField(max_length=30, unique=True)
    sexo = models.ForeignKey(Sexo, on_delete=models.PROTECT)
    telefono = models.CharField(max_length=15, blank=True)
    cel = models.CharField(max_length=15, default=8090000000, blank=True)
    otro = models.CharField(max_length=15, default=8090000000, blank=True)
    email = models.EmailField(blank=True, verbose_name='e-mail')
    leader = models.ForeignKey('self', null=True,
                               on_delete=models.SET_NULL, limit_choices_to={'es_lider': True})
    foto = models.ImageField(upload_to='consolidadores', blank=True)
    etapas = models.ManyToManyField(Etapa)
    es_lider = models.BooleanField(blank=True)

    estados_civiles = models.ManyToManyField(Civil)

    fecha_creacion = models.DateField(auto_now_add=True, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    objects = models.Manager()
    active_objects = managers.ConsolidadorManager()

    class Meta:
        ordering = ['nombre']
        verbose_name_plural = "Consolidadores"

    """
    esto es para poder acceder a el lider anterior cuando
    se esta actualizando la información del consolidador,
    de manera que se pueda comparar si el campo lider
    fue cambiado
    """

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('actualizar-consolidador', kwargs={'pk': self.pk})

    def get_lider(self):
        return self.leader

    def as_leader_id(self):
        return self.id if self.es_lider else None

    # TODO: contact_info property can be from abstract class to both, consolidador and creyente
    @property
    def contact_info(self):
        return f"{f'{self.telefono}{linesep}' if self.telefono != '8090000000' else ''}" \
               f"{f'{self.cel}{linesep}' if self.cel != '8090000000' else ''}" \
               f"{f'{self.otro}{linesep}' if self.otro != '8090000000' else ''}" \
               f"{self.email}{linesep if self.email else ''}"


class Celula(models.Model):
    consolidador = models.ForeignKey(Consolidador, related_name='lider_celula', null=True, blank=True,
                                     on_delete=models.CASCADE)
    address = models.TextField(blank=True, null=True)
    location = models.CharField(default='-69.908897,18.535497', verbose_name='Ubicación', max_length=50, blank=False)
    dia = models.ForeignKey(Dia, related_name='dia_de_reunion', null=True, blank=True,
                            on_delete=models.CASCADE, default=None)
    hora = models.TimeField()
    frecuencia = models.ForeignKey(Frecuencia, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    prox_reporte = models.DateField(null=True, blank=True)
    nota = models.TextField(blank=True, null=True)
    fecha_creacion = models.DateField(auto_now_add=True, null=True, blank=True)

    @property
    def longitud(self):
        print(self.location)
        return self.location.split(',')[0]

    @property
    def latitude(self):
        return self.location.split(',')[1]

    def __str__(self):
        return "Líder: " + self.consolidador.nombre

    def reunion(self):
        return "Reuniones "+str(self.frecuencia)+". "+str(self.dia)+" a las "+self.hora.strftime("%I:%M%p")


class Creyente(models.Model):
    nombre = models.CharField(max_length=30, verbose_name='Nombre Completo')
    apodo = models.CharField(blank=True, max_length=15)
    direccion = models.CharField(max_length=200)
    location = models.CharField(default='-69.908897,18.535497', verbose_name='Ubicación', max_length=50, blank=False)
    cedula = models.CharField(blank=True, max_length=11)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    foto = models.ImageField(upload_to='creyentes', blank=True)

    edad = models.IntegerField()
    etapa = models.ForeignKey(Etapa, on_delete=models.PROTECT)
    telefono = models.CharField(blank=True, default=8090000000, max_length=10)
    celular = models.CharField(blank=True, default=8090000000, max_length=10)
    otro = models.CharField(blank=True, max_length=10)
    email = models.EmailField(blank=True, verbose_name='e-mail')

    estado_laboral = models.ManyToManyField(Laboral, blank=True)
    ocupacion = models.ManyToManyField(Ocupacion, blank=True)
    estado_civil = models.ForeignKey(Civil, on_delete=models.SET_NULL, blank=True, null=True)

    sexo = models.ForeignKey(Sexo, on_delete=models.PROTECT, blank=True, null=True)
    fecha_culto = models.DateField(null=True, blank=True)
    dia = models.ForeignKey(Dia, on_delete=models.PROTECT, blank=True, null=True)
    actividad = models.ForeignKey(Actividad, on_delete=models.SET_DEFAULT, default=11)
    razon = models.ForeignKey(Razon, on_delete=models.SET_NULL, blank=True, null=True)

    consolidador = models.ForeignKey(Consolidador, related_name='creyente_consolidador', null=True, blank=True,
                                     on_delete=models.CASCADE)
    celula = models.ForeignKey(Celula, related_name='celula', null=True, blank=True, on_delete=models.CASCADE,
                               default=None)

    reunion = models.TextField(blank=True, null=True)
    nota = models.TextField(blank=True, null=True)

    bautizado = models.BooleanField(default=False, blank=True)
    encuentro = models.BooleanField(default=False, blank=True)
    reencuentro = models.BooleanField(default=False, blank=True)
    escuela_lideres = models.BooleanField(default=False, blank=True)
    status = models.IntegerField(choices=CreyenteStatus.choices, default=CreyenteStatus.ACTIVE)

    fecha_creacion = models.DateField(auto_now_add=True, null=True, blank=True)

    objects = managers.CreyenteQuerySet().as_manager()

    class Meta:
        ordering = ['nombre']
        unique_together = (("nombre", "telefono", "celular"),)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.id:
            self.nombre = self.nombre.strip().title()
            self.etapa = calc_etapa(Etapa, self.edad)

        super(Creyente, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('actualizar-creyente', kwargs={'pk': self.pk})

    @property
    def longitud(self):
        return self.location.split(',')[0]

    @property
    def latitude(self):
        return self.location.split(',')[1]

    @property
    def contact_info(self):
        info = f"{f'{self.telefono}, ' if self.telefono != '8090000000' else ''}" \
               f"{f'{self.celular}, ' if self.celular != '8090000000' else ''}" \
               f"{f'{self.otro}, ' if self.otro != '8090000000' else ''}" \
               f"{self.email}"

        return info[:-2] if info.endswith(', ') else info


class Iglesia(models.Model):

    def __str__(self):
        return self.nombre

    nombre = models.CharField(max_length=100)
    pais = CountryField()
    sector = models.CharField(max_length=100)
    foto = models.ImageField(upload_to='iglesias', blank=True)
    # TODO: limit_choices_to is_pastor
    pastor = models.ForeignKey(Consolidador, on_delete=models.SET_NULL, null=True, limit_choices_to={'es_lider': True})

    def get_absolute_url(self):
        return reverse('agregar-iglesia')
