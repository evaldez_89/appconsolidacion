from enum import Enum
from django.db.models import IntegerChoices


class Access(Enum):
    FULL = 7
    STAFF = 6
    LIDER_MINISTERIO = 5
    MINISTERIO = 3
    LIDER = 2
    CONSOLIDADOR = 1


class CreyenteStatus(IntegerChoices):
    PROMOTED = 3,
    CONSOLIDATED = 2,
    ACTIVE = 1,
    DELETED = 0,
