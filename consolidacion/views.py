#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import date
from datetime import datetime, timedelta

import xlrd
import xlwt
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render
from django.urls import reverse

from Funciones import get_hoy, crear_usuario as create_user, enviaMail, user_has_access, add_group
from consolidacion import models
from consolidacion.decorators import can_edit_person_info
from consolidacion.enumerables import Access, CreyenteStatus
from consolidacion.forms import AddCreyente, AddConsolidador, AddCelula, AddNoCreyente
from consolidacion.templatetags.etiquetas import getLiderId, totalCreyentes, class_name


@login_required(login_url='/login')
def form_buscar(request):
    return render(request, 'form_buscar.html')


@login_required(login_url='/login')
def crear_usuario(request, pk):
    consolidador = models.Consolidador.active_objects.get(id=int(pk))
    if consolidador.email:
        create_user(consolidador)
        return HttpResponseRedirect(f'/buscar_consolidador/?q={consolidador.nombre}')
    else:
        return HttpResponseRedirect(f'/consolidador/{pk}/editar')


@login_required(login_url='/login')
def deactivate_consolidador(request, pk):
    if user_has_access(request.user, Access.MINISTERIO) or request.user.is_staff:
        consolidador = models.Consolidador.active_objects.get(id=int(pk))
        consolidador.is_active = False
        consolidador.save()
    return HttpResponseRedirect('/ver_consolidadores')


@login_required(login_url='/login')
def delete_consolidador(request, pk):
    if user_has_access(request.user, Access.MINISTERIO) or request.user.is_staff:
        consolidador = models.Consolidador.active_objects.get(id=int(pk))
        consolidador.is_deleted = True
        consolidador.is_active = False
        consolidador.save()
    return HttpResponseRedirect('/ver_consolidadores')


@login_required(login_url='/login')
def add_group_to_user(request, pk):
    # todo: right now the group set by default to be "ministerio" but its not supposed to
    group = "ministerio"
    consolidador = models.Consolidador.active_objects.get(id=int(pk))

    message = "Usuario No Pudo Ser Creado"

    message_class = "warning"

    if add_group(consolidador.nombre, group):
        message = "Usuario Creado Satisfactoriamente"
        message_class = "success"

    return HttpResponseRedirect('/detalles_consolidador/?id=' + pk)


@can_edit_person_info
@login_required(login_url='/login')
def agregarNota(request, pk):
    creyente = models.Creyente.objects.get(id=int(pk))
    if 'comment' in request.POST and len(request.POST["comment"]) > 5:
        nota = request.POST['comment'].replace("\n", " ").replace("\r", " ")
        creyente.nota += "\n" + request.user.first_name + ' ' + get_hoy() + ":\n" + nota + '\r\n'
        creyente.save()
    return HttpResponseRedirect("/detalles_creyente/?id=" + pk)


@login_required(login_url='/login')
def buscar(request, model):
    errors = []
    creyentes = list()
    prefix = ''
    page = ''
    obj_model_name = class_name(model)

    if 'page' in request.GET:
        page = request.GET['page']

    if user_has_access(request.user, Access.MINISTERIO):
        if 'q' in request.GET:
            q = request.GET['q']
            prefix = f'?q={q}&'
            if not q:
                errors.append("Por favor introduce un término de búsqueda.")
            elif len(q) > 20:
                errors.append("Por favor introduce un término de búsqueda menor a 20 caracteres.")
            else:
                resultados = model.objects.filter(nombre__icontains=q)
                for creyente in resultados:
                    creyentes.append(creyente)

                paginator = Paginator(creyentes, 20)

                try:
                    object_list = paginator.page(page)
                except PageNotAnInteger:
                    object_list = paginator.page(1)
                except EmptyPage:
                    object_list = paginator.page(paginator.num_pages)
                modelo = obj_model_name
                return render(request, modelo.lower() + '_list.html', {'q': q,
                                                                       'hoy': get_hoy,
                                                                       'aviso': errors,
                                                                       'object_list': object_list,
                                                                       'tipo': 'siguiendo',
                                                                       'prefix': prefix})
    else:
        errors = ["Favor comunicarse con su lider"]

    return render(request, 'form_buscar.html', {'errors': errors, 'model': obj_model_name})


@login_required(login_url='/login/?next=/add_creyente/')
def add_creyente(request):
    if user_has_access(request.user, Access.MINISTERIO):
        form = AddCreyente(request.POST or None, request.FILES or None)
        opt_procesos = {}
        if request.method == 'POST':
            if form.is_valid():
                agregar = form.save()
                if "hoy" in request.POST:
                    if request.POST["hoy"] is True or request.POST["hoy"] == "on":
                        agregar.fecha_culto = date.today().strftime("%Y-%m-%d")

                agregar.nombre = request.POST['nombre'].strip().title()
                agregar.apodo = request.POST['apodo'].strip().title()
                agregar.direccion = request.POST['direccion'].strip().title()

                if "foto" in request.FILES:
                    agregar.foto = request.FILES['foto']
                else:
                    agregar.foto = None

                if 'telefono' in request.POST and len(request.POST['telefono']) > 0:
                    agregar.telefono = request.POST['telefono']
                else:
                    agregar.telefono = '8090000000'

                if 'celular' in request.POST and len(request.POST['celular']) > 0:
                    agregar.celular = request.POST['celular']
                else:
                    agregar.celular = '8090000000'

                if 'otro' in request.POST and len(request.POST['otro']) > 0:
                    agregar.otro = request.POST['otro']
                else:
                    agregar.otro = '8090000000'

                agregar.email = request.POST['email'].strip()
                agregar.reunion = request.POST['reunion'].strip()
                nota = request.POST['nota'].replace("\n", " ").replace("\r", " ").strip()
                agregar.nota = 'Creyente Agregado por ' + request.user.first_name + ' ' + str(
                    get_hoy()) + ':\n' + nota + '\r\n'
                if int(request.POST['edad']) > 12:
                    agregar.edad = int(request.POST['edad'])

                agregar.sexo.id = request.POST['sexo']
                agregar.estado_civil.id = request.POST['estado_civil']

                agregar.bautizado = request.POST.get('bautizado') == 'on'
                agregar.encuentro = request.POST.get('encuentro') == 'on'
                agregar.reencuentro = request.POST.get('reencuentro') == 'on'
                agregar.escuela_lideres = request.POST.get('escuela_lideres') == 'on'

                # Many to Many fields

                if 'ocupacion' in request.POST and len(request.POST['ocupacion']) > 0:
                    agregar.ocupacion.set(request.POST['ocupacion'])
                if 'estado_laboral' in request.POST and len(request.POST['estado_laboral']) > 0:
                    agregar.estado_laboral.set(request.POST['estado_laboral'])

                agregar.save()
                global name
                name = str(agregar.nombre)

                enviaMail(categoria=3, usuario=request.user.first_name, creyente=agregar)

                new_id = models.Creyente.objects.get(nombre=agregar.nombre).pk

                return HttpResponseRedirect(reverse("consolidadores-disponibles", args=[new_id]))
        return render(request, 'form_add.html', {'form': form, 'model': 'creyente', 'opt': opt_procesos})
    return HttpResponseNotFound('<h3>Contactar al personal de soporte consolidación</h3>')


@login_required(login_url='/login/?next=/add_creyente/')
def add_no_creyente(request, pk):
    celula = models.Celula.objects.get(id=int(pk))

    if user_has_access(request.user, Access.MINISTERIO):
        form = AddNoCreyente(request.POST or None, request.FILES or None)

        if request.method == 'POST':
            if form.is_valid():
                agregar = form.save()

                agregar.nombre = request.POST['nombre']
                agregar.apodo = request.POST['apodo']
                agregar.direccion = request.POST['direccion']

                if "foto" in request.FILES:
                    agregar.foto = request.FILES['foto']
                else:
                    agregar.foto = None

                if 'telefono' in request.POST and len(request.POST['telefono']) > 0:
                    agregar.telefono = request.POST['telefono']
                else:
                    agregar.telefono = '8090000000'

                if 'celular' in request.POST and len(request.POST['celular']) > 0:
                    agregar.celular = request.POST['celular']
                else:
                    agregar.celular = '8090000000'

                if 'otro' in request.POST and len(request.POST['otro']) > 0:
                    agregar.otro = request.POST['otro']
                else:
                    agregar.otro = '8090000000'

                agregar.email = request.POST['email']

                if 'reunion' in request.POST:
                    agregar.reunion = request.POST['reunion']

                if 'nota' in request.POST:
                    nota = request.POST['nota'].replace("\n", " ").replace("\r", " ")
                    agregar.nota = 'Creyente Agregado por ' + request.user.first_name + ' ' + str(
                        get_hoy()) + ':\n' + nota + '\r\n'

                if int(request.POST['edad']) > 12:
                    agregar.edad = int(request.POST['edad'])

                agregar.sexo.id = request.POST['sexo']
                agregar.estado_civil.id = request.POST['estado_civil']

                agregar.reunion = celula.reunion()
                agregar.celula = celula

                # Many to Many fields

                if 'ocupacion' in request.POST and len(request.POST['ocupacion']) > 0:
                    agregar.ocupacion = request.POST['ocupacion']
                if 'estado_laboral' in request.POST and len(request.POST['estado_laboral']) > 0:
                    agregar.estado_laboral = request.POST['estado_laboral']

                agregar.save()

        return render(request, 'add_no_creyente.html', {'form': form})
    return HttpResponseNotFound('<h3>Contactar al personal de soporte consolidación</h3>')


@login_required(login_url='/login/?next=/add_consolidador/')
def add_consolidador(request):
    if user_has_access(request.user, Access.MINISTERIO):
        form = AddConsolidador(request.POST or None, request.FILES or None)
        if request.method == 'POST':
            if form.is_valid():
                agregar = form.save()

                agregar.nombre = request.POST['nombre']
                if 'foto' in request.FILES:
                    agregar.foto = request.FILES['foto']
                else:
                    agregar.foto = None

                if 'telefono' in request.POST and len(request.POST['telefono']) > 0:
                    agregar.telefono = request.POST['telefono']
                else:
                    agregar.telefono = '8090000000'

                if 'cel' in request.POST:
                    agregar.cel = request.POST['cel']
                else:
                    agregar.cel = '8090000000'

                if 'otro' in request.POST:
                    agregar.otro = request.POST['otro']
                else:
                    agregar.otro = '8090000000'

                if 'email' in request.POST:
                    agregar.email = request.POST['email']

                agregar.es_lider = request.POST.get('es_lider') == 'on'

                agregar.sexo.id = request.POST['sexo']
                agregar.etapas.id = request.POST['etapas']
                agregar.estados_civiles.id = request.POST['estados_civiles']

                agregar.save()
                consolidador = models.Consolidador.active_objects.get(nombre=agregar.nombre)
                return HttpResponseRedirect('/detalles_consolidador/?id=' + str(consolidador.id))

        return render(request, 'form_add.html', {'form': form, 'model': 'consolidador'})
    return HttpResponseNotFound('<h3>Contactar al personal de soporte consolidación</h3>')


@login_required(login_url='/login/?next=/add_consolidador/')
def add_celula(request):
    if user_has_access(request.user, Access.MINISTERIO):
        form = AddCelula(request.POST or None)
        if request.method == 'POST':
            if form.is_valid():
                agregar = form.save()

                agregar.consolidador.id = request.POST['consolidador']
                agregado.ubicacion = request.POST['ubicacion']
                agregar.dia.id = request.POST['dia']
                agregar.hora = request.POST['hora']
                agregar.frecuencia.id = request.POST['frecuencia']
                agregar.prox_reporte = request.POST['prox_reporte']
                agregar.nota = request.POST['nota']

                idc = request.POST['consolidador']

                agregar.save()
                return HttpResponseRedirect('/detalles_consolidador/?id=' + str(idc))
        return render(request, 'add_celula.html', {'form': form})
    return HttpResponseNotFound('<h3>Contactar al personal de soporte consolidación</h3>')


@login_required(login_url='/login/?next=/disp_consolidador/')
def show_consolidadores(request, pk):
    aviso = 'Para esta acción, favor comunicarse con el personal de Consolidación.'
    consolidadores = []
    creyente = models.Creyente.objects.get(id=pk)

    if user_has_access(request.user, Access.MINISTERIO):
        aviso = ''

        lista = models.Consolidador.active_objects.filter(sexo=creyente.sexo).exclude(leader_id__in=[6, 20]).distinct()
        for consolidador in lista:
            consolidando = totalCreyentes(consolidador.id)
            if creyente.etapa in consolidador.etapas.all() and \
                    creyente.estado_civil in consolidador.estados_civiles.all() and consolidando < 9:
                consolidadores.append(consolidador)
        if len(consolidadores) == 0:
            aviso = 'No hay consolidadores disponibles para ' + str(creyente.nombre)

    else:
        # get consolidadores assign to the current user
        consolidadores = models.Consolidador.active_objects.filter(leader__nombre=request.user.first_name)

    if len(consolidadores) > 0:
        aviso = ''

    return render(request, 'set_consolidador.html', {'object_list': consolidadores,
                                                     'creyente': creyente,
                                                     'aviso': aviso})


@login_required(login_url='/login/?next=/disp_lideres/')
def show_lideres(request, pk):
    aviso = 'Para esta acción, favor comunicarse con el personal de Consolidación.'
    consolidador = models.Consolidador.active_objects.get(id=pk)

    if user_has_access(request.user, Access.MINISTERIO):
        aviso = ''

        consolidadores_lideres = models.Consolidador.active_objects.filter(es_lider=True).exclude(leader_id__in=[6, 20]).distinct()

        if len(consolidadores_lideres) == 0:
            aviso = 'No hay consolidadores disponibles para ' + str(consolidador.nombre)

    else:
        # get consolidadores assign to the current user
        consolidadores_lideres = models.Consolidador.active_objects.filter(leader__nombre=request.user.first_name)

    if len(consolidadores_lideres) > 0:
        aviso = ''

    return render(request, 'set_lider.html', {'lista': consolidadores_lideres,
                                              'consolidador': consolidador,
                                              'aviso': aviso})


@login_required(login_url='/login/?next=/disp_consolidador/')
def showCelulas(request):
    aviso = 'Para esta acción, favor comunicarse con el personal de Consolidación.'
    celulas = []
    creyente = None

    if 'nombre' in request.GET or 'id' in request.GET:
        if 'nombre' in request.GET:
            creyente = models.Creyente.objects.get(nombre=request.GET['nombre'])
        else:
            creyente = models.Creyente.objects.get(id=int(request.GET['id']))

    if user_has_access(request.user, Access.LIDER_MINISTERIO):
        aviso = ''

        # listado basado en el sexo
        lista = models.Celula.objects.filter(consolidador__sexo=creyente.sexo)
        for celula in lista:
            asistencia = totalCreyentes(celula.id, True)
            celulas.append(celula)
    else:
        # get celulas of the current user
        celulas = models.Consolidador.active_objects.filter(leader__nombre=request.user.first_name)

    if len(celulas) > 0:
        aviso = ''

    return render(request, 'set_celula.html', {'lista': celulas,
                                               'creyente': creyente,
                                               'aviso': aviso})


@login_required(login_url='/login/?next=/disp_consolidador/')
def showLideres(request, pk):
    aviso = 'Para esta acción, favor comunicarse con el personal de Consolidación.'
    creyente = models.Creyente.objects.filter(id=pk).first()
    lista = list()

    if creyente is not None:
        if user_has_access(request.user, Access.LIDER_MINISTERIO):
            aviso = ''
            lista = models.Consolidador.active_objects.filter(es_lider=True).filter(sexo=creyente.sexo)

            if len(lista) == 0:
                aviso = 'No hay consolidadores disponibles para ' + str(creyente.nombre)
            else:
                aviso = 'Lideres'

        return render(request, 'set_consolidador.html', {'lista': lista,
                                                         'creyente': creyente,
                                                         'aviso': aviso})
    else:
        aviso = "Al parecer hubo un error. Contacte al personal de soporte para corregir esto."
        return render(request, 'set_consolidador.html', {'lista': None,
                                                         'creyente': 'Error En el ID',
                                                         'aviso': aviso})


@login_required(login_url='/login/?next=/disp_consolidador/')
def set_consolidador(request, pk, consolidador_id=None):
    if user_has_access(request.user, Access.MINISTERIO):
        creyente = models.Creyente.objects.get(pk=pk)

        if consolidador_id:
            nuevo = models.Consolidador.active_objects.get(id=consolidador_id)

            creyente.consolidador = nuevo

            creyente.nota += f"\n{request.user.first_name}  {get_hoy()}:\nconsolidador asignado {nuevo.nombre}\r\n"
            creyente.save()

            enviaMail(categoria=2, consolidador=nuevo, creyente=creyente)

            return render(request, 'creyente_agregado.html', {'creyente': creyente})
        else:
            return render(request, 'set_consolidador.html',
                          {'lista': None,
                           'creyente': creyente,
                           'aviso': 'Debe Elegir Un Consolidador'})
    else:
        return HttpResponseRedirect('/buscar_creyente')


@login_required(login_url='/login/?next=/disp_consolidador/')
def set_lider(request, offset):
    if user_has_access(request.user, Access.MINISTERIO):
        consolidador = models.Consolidador.active_objects.get(id=offset)

        if 'lider' in request.POST:
            lider = models.Consolidador.active_objects.get(id=request.POST['lider'])

            consolidador.leader = lider

            consolidador.save()

            return HttpResponseRedirect(f'/detalles_consolidador/?id={consolidador.id}')
        else:
            return render(request, 'set_lider.html',
                          {'lista': None,
                           'consolidador': consolidador,
                           'aviso': 'Debe Elegir Un Consolidador'})
    else:
        return HttpResponseRedirect('/ver_consolidadores')


@login_required(login_url='/login/?next=/disp_consolidador/')
def set_creyentes(request, pk):
    if user_has_access(request.user, Access.MINISTERIO):

        consolidador = models.Consolidador.active_objects.get(pk=pk)

        if 'creyentes' in request.POST:
            for creyente_id in request.POST.getlist('creyentes'):
                creyente = models.Creyente.objects.get(id=creyente_id)
                if creyente is not None:
                    creyente.consolidador = consolidador
                    creyente.nota += f"\n{request.user.first_name} {get_hoy()}:\n" \
                                     f"consolidador asignado directamente {consolidador.nombre}"
                    creyente.save()

            enviaMail(categoria=4, consolidador=consolidador)

        return HttpResponseRedirect(f"/ver_creyentes/?idc={consolidador.id}")

    else:
        return HttpResponseRedirect('/buscar_creyente')


@login_required(login_url='/login/?next=/ver_creyentes/')
def available_creyentes(request, pk):
    # TODO: Given a consolidador pk, show all creyentes without consolidador
    #       in order to be able to assign multiple creyentes to a single consolidador.
    #       This is going to be something similar to set_consolidador, but instead of
    #       showing multiple consolidadores availables for a creyente, its going to show
    #       multiple creyentes available for a consolidador.
    if user_has_access(request.user, Access.MINISTERIO):

        aviso = ''
        consolidador = models.Consolidador.active_objects.get(id=pk)

        creyentes = models.Creyente.objects.filter(consolidador=None,
                                                   sexo=consolidador.sexo,
                                                   etapa__in=consolidador.etapas.all(),
                                                   estado_civil__in=consolidador.estados_civiles.all())

        if creyentes.count() == 0:
            aviso = 'No hay creyentes disponibles para ' + str(consolidador.nombre)

        return render(request, 'disponibilidad_creyentes.html', {'lista': creyentes, 'consolidador': consolidador, 'aviso': aviso})

    else:
        raise PermissionDenied()


# def set_creyentes(request, offset):
#     pass


@login_required(login_url='/login/?next=/disp_consolidador/')
def setCelula(request, offset):
    if user_has_access(request.user, Access.CONSOLIDADOR):
        offset = int(offset)
        creyente = models.Creyente.objects.get(id=offset)

        if 'celula' in request.POST:
            nueva = models.Celula.objects.get(id=request.POST['celula'])
            creyente.celula = nueva
            creyente.nota += "\n" + request.user.first_name + " " + get_hoy() + \
                             ':\nCreyente agregado a la célula del ' + nueva.consolidador.nombre + '\r\n'
            creyente.save()

            return render(request, 'creyente_agregado.html', {'creyente': creyente})
        else:
            return render(request, 'set_celula.html', {'lista': None,
                                                       'creyente': '',
                                                       'aviso': 'Algo anda mal. Contacte al personal de soporte.'})
    else:
        return HttpResponseRedirect('/buscar_creyente')


@login_required(login_url='/login/?next=/ver_creyentes/')
def lista_creyentes(request, tipo):
    lista = list()
    prefix = "?"
    aviso = ""
    page = request.GET.get('page', 1)
    qty_by_page = 20

    # Si es lider o del ministerio de consolidación, puede consultar cualquier consolidador o lider
    if user_has_access(request.user, Access.LIDER):
        # Muestra todos los creyentes en la red de un lider
        if 'lider_id' in request.GET:
            # Get the lider info
            query_lider = models.Consolidador.active_objects.filter(id=int(request.GET['lider_id']), es_lider=True).first()

            # Try to get the Consolidador instance of the lider
            # if its a consolidación staff, show any lider, else only if the lider himself
            if user_has_access(request.user, Access.MINISTERIO) or request.user.first_name == query_lider.nombre:
                lider_as_consolidador = models.Consolidador.active_objects.filter(nombre=query_lider.nombre).first()
                aviso = "Mostrado Creyentes Asignados a " + query_lider.nombre

                lista = models.Creyente.objects.lider_network(query_lider)

                # Paginator prefix
                prefix = f'?lider_id={query_lider.id}&'
            else:
                aviso = 'No hay lider con ese id'
        elif 'idc' in request.GET:
            # consolidador: nombre o ID
            try:
                consolidador = models.Consolidador.active_objects.get(id=int(request.GET['idc']))
            except ValueError:
                consolidador = models.Consolidador.active_objects.get(nombre=request.GET['idc'])
            aviso = "Mostrado Creyentes Asignados a " + consolidador.nombre

            lista = models.Creyente.objects.by_consolidador(consolidador.id)

            # Paginator prefix
            prefix = f'?idc={consolidador.id}&'
        else:
            aviso = models.Creyente.objects.by_status(tipo)[0]
            lista = models.Creyente.objects.by_status(tipo)[1]
    else:
        if user_has_access(request.user, Access.CONSOLIDADOR):
            consolidador = models.Consolidador.active_objects.get(nombre=request.user.first_name)
            aviso = "Mostrado Creyentes Asignados a " + request.user.first_name

            lista = models.Creyente.objects.by_consolidador(consolidador.id)

            prefix = f'?idc={consolidador.id}&'

    # Solo devuelve la lista (ahora mismo solo se usa para convertir a xls)
    paginator = Paginator(lista, qty_by_page)

    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        object_list = paginator.page(1)
    except EmptyPage:
        object_list = paginator.page(paginator.num_pages)

    return render(request, 'creyente_list.html', {'hoy': get_hoy, 'aviso': aviso,
                                                  'object_list': object_list,
                                                  'tipo': tipo, 'prefix': prefix})


@login_required(login_url='/login/?next=/ver_creyentes/')
def agruparCreyentes(request, group_by):
    fecha_init = datetime.strptime("2016-08-31", '%Y-%m-%d')

    aviso = []
    culto_nulo = []
    lista = models.Creyente.objects.by_status('siguiendo')
    agrupar = group_by

    if user_has_access(request.user, Access.LIDER):
        if 'agrupar_x' in request.GET:
            agrupar = request.GET['agrupar_x']
        else:
            aviso.append('No hay grupo para esta categoría')

    # revisar, puede ser util
    while fecha_init < datetime.today():
        fecha_init = fecha_init + timedelta(days=1)
        if fecha_init.weekday() == 3 or fecha_init.weekday() == 6:
            if lista[1].filter(fecha_culto=fecha_init).count() == 0:
                culto_nulo.append(fecha_init)

    return render(request, 'creyente_agrupado.html', {'hoy': get_hoy, 'aviso': aviso,
                                                      'object_list': lista[1].order_by("-" + agrupar),
                                                      'agrupar': agrupar, 'culto_nulo': culto_nulo})


@login_required(login_url='/login/?next=/ver_consolidadores/')
def listaConsolidadores(request):
    if request.user.is_authenticated:
        lista = list()
        aviso = ''
        prefix = "?"
        page = request.GET.get('page', 1)
        if user_has_access(request.user, Access.LIDER_MINISTERIO):
            if 'idc' in request.GET:
                lista = models.Consolidador.active_objects.filter(leader_id=int(request.GET['idc']))
            else:
                lista = models.Consolidador.active_objects.all()
        elif user_has_access(request.user, Access.LIDER):
            aviso = "Mostrando Consolidadores Asignados a " + request.user.first_name
            lista = models.Consolidador.active_objects.all().filter(leader__nombre=request.user.first_name)
        else:
            aviso = request.user.first_name + " No tiene consolidadores asignados \n o no tienes permisos de lider"

        paginator = Paginator(lista, 20)

        try:
            object_list = paginator.page(page)
        except PageNotAnInteger:
            object_list = paginator.page(1)
        except EmptyPage:
            object_list = paginator.page(paginator.num_pages)

        return render(request, 'consolidador_list.html',
                      {'aviso': aviso, 'object_list': object_list, 'prefix': prefix})


@login_required(login_url='/login/?next=/ver_consolidadores/')
def listalideres(request):
    if request.user.is_authenticated:
        lista = list()
        aviso = ''
        prefix = "?"
        page = request.GET.get('page', 1)
        if user_has_access(request.user, Access.LIDER_MINISTERIO):
            lista = models.Consolidador.active_objects.filter(es_lider=True).all()
        else:
            aviso = request.user.first_name + " No tiene consolidadores asignados \n o no tienes permisos de lider"

        paginator = Paginator(lista, 20)

        try:
            object_list = paginator.page(page)
        except PageNotAnInteger:
            object_list = paginator.page(1)
        except EmptyPage:
            object_list = paginator.page(paginator.num_pages)

        return render(request, 'lider_list.html',
                      {'aviso': aviso, 'object_list': object_list, 'prefix': prefix})


@login_required(login_url='/login/?next=/ver_creyentes/')
def listaCelula(request):
    lista = []
    aviso = ''

    if request.user.is_authenticated:
        if user_has_access(request.user, Access.LIDER):
            lista = models.Celula.objects.all()
        else:
            if request.user.groups.filter(name='lider'):
                # obtener todos los consolidadores asignados al lider que esta en sesión
                consolidadores = models.Consolidador.active_objects.filter(
                    leader_id=getLiderId(request.user.first_name)).values("id")

                for discipulos in consolidadores:
                    # agregar células de consolidadores asignados
                    lista += models.Celula.objects.filter(consolidador_id=discipulos['id'])

                # agregar células propias
                idDeConsolidador = models.Consolidador.active_objects.values('id').get(nombre=request.user.first_name)
                lista += (models.Celula.objects.filter(consolidador_id=idDeConsolidador['id']))
            elif request.user.groups.filter(name='consolidador'):
                consolidador = models.Consolidador.active_objects.values('id').get(nombre=request.user.first_name)
                lista = models.Celula.objects.filter(consolidador_id=consolidador['id'])

        return render(request, 'celula_list.html', {'aviso': aviso, 'object_list': lista})


@login_required(login_url='/login/?next=/ver_creyentes/')
def agregado(request):
    creyente = models.Creyente.objects.get(nombre=name)
    return render(request, 'creyente_agregado.html', {'creyente': creyente})


@login_required(login_url='/login/?next=/ver_creyentes/')
def Plantilla(request, model, template):
    # send lider id to to plantilla creyente in order to enable a button to change consolidador
    lider_id = models.Consolidador.active_objects.filter(nombre=request.user.first_name, es_lider=True).first()

    # puede ser creyente, consolidador o celula
    entidad = None
    if lider_id is None:
        lider_id = 0
    else:
        lider_id = lider_id.id

    # get the person from id or name
    if 'id' in request.GET:
        entidad = model.objects.get(id=int(request.GET['id']))
    if 'nombre' in request.GET:
        entidad = model.objects.get(nombre=request.GET['nombre'])

    model_name = model._meta.model_name

    if model_name != 'celula':

        can_view_profile = any(
            [
                # If the request is for a creyente, verify if the user is the consolidador of that creyente
                request.user.first_name == entidad.consolidador.nombre if model_name == "creyente" and entidad.consolidador is not None else False,
                # if the request is for a consolidador, verify if the user is the same as the consolidador
                request.user.first_name == entidad.nombre if model_name == "consolidador" else False,
                # If the request is for a lider, verify if the its the lider of the user requesting
                request.user.first_name in entidad.consolidador_set.all().values_list("nombre", flat=True) if model_name == "lider" else False
            ]
        )

        if user_has_access(request.user, Access.LIDER) or can_view_profile:
            return render(request, template, {entidad._meta.model_name: entidad, 'modelo': model,
                                              "hoy": get_hoy, 'lider_id': lider_id})

        else:
            return HttpResponseNotFound('<h1>Usted No tiene Acceso</h1>')
    else:
        # muestra el detalle de la celula del consolidador en sesion
        lista = models.Creyente.objects.filter(celula=entidad)
        if request.user.is_staff or (request.user.groups.filter(name="lider").exists()) \
                or (request.user.groups.filter(name="ministerio").exists()):
            return render(request, template, {entidad._meta.model_name: entidad,
                                              'modelo': model, "hoy": get_hoy,
                                              'lista': lista})


@login_required(login_url='/login/?next=/ver_creyentes/')
def baja(request, pk):
    creyente = models.Creyente.objects.get(id=int(pk))
    razones = models.Razon.objects.all()
    try:
        nota = creyente.nota.split('\n')[-2] + "\n" + creyente.nota.split('\n')[-1]
    except IndexError:
        nota = creyente.nota

    return render(request, 'form_debaja.html', {'creyente': creyente, 'razones': razones, 'nota': nota})


@login_required(login_url='/login/?next=/ver_creyentes/')
def de_baja(request, offset):
    creyente = models.Creyente.objects.get(id=int(offset))
    if user_has_access(request.user, Access.LIDER_MINISTERIO):
        if len(request.POST['nota']) > 0:
            nota = request.POST['nota'].replace("\n", " ").replace("\r", " ")
            creyente.nota += '\n' + request.user.first_name + ' ' + get_hoy() + ":\n" + nota + '\r\n'
        creyente.status = CreyenteStatus.DELETED
        creyente.save()

        return render(request, 'de_baja.html', {'creyente': creyente, 'consolidador': creyente.consolidador})
    else:
        raise PermissionDenied()


@login_required(login_url='/login/?next=/ver_creyentes/')
def consolidado(request, pk):
    creyente = models.Creyente.objects.get(id=int(pk))
    return render(request, 'form_consolidado.html', {'creyente': creyente})


@login_required(login_url='/login/?next=/ver_creyentes/')
def creyente_consolidado(request, offset):
    if user_has_access(request.user, Access.LIDER_MINISTERIO):
        creyente = models.Creyente.objects.get(id=int(offset))
        if 'nota' in request.POST:
            nota = request.POST['nota'].replace("\n", " ").replace("\r", " ")
            creyente.nota += '\n' + request.user.first_name + ' ' + get_hoy() + ":\n" + nota + '\r\n'

        creyente.status = CreyenteStatus.CONSOLIDATED
        creyente.save()

        # Notificar Consolidación completada
        enviaMail(categoria=6, usuario=request.user.first_name, creyente=creyente)
        return render(request, 'creyente_consolidado.html', {'creyente': creyente, 'consolidador': creyente.consolidador})
    else:
        raise PermissionDenied()


@login_required(login_url='/login/?next=/ver_creyentes/')
def promover_consolidador(request, nombre):
    if user_has_access(request.user, Access.LIDER_MINISTERIO):
        # trata de obtener cada uno de los modelos: Consolidador, Líder
        consolidador = models.Consolidador.active_objects.filter(nombre=nombre).first()

        if consolidador is not None:
            consolidador.es_lider = True
            consolidador.save()

        url = '/detalles_consolidador/?id=%s' % consolidador.id

        return HttpResponseRedirect(url)
    else:
        raise PermissionDenied()


@login_required(login_url='/login/?next=/ver_creyentes/')
def promover_creyente(request, nombre):
    if user_has_access(request.user, Access.LIDER_MINISTERIO):
        creyente = models.Creyente.objects.filter(nombre=nombre).first()
        consolidador = models.Consolidador.active_objects.filter(nombre=nombre).first()

        # Tratar de crear un consolidador
        if consolidador is None:
            # Crear nuevo Consolidador
            consolidador = models.Consolidador.active_objects.create(
                nombre=creyente.nombre,
                sexo=creyente.sexo,
                telefono=creyente.telefono,
                cel=creyente.celular,
                otro=creyente.otro,
                email=creyente.email,
                foto=creyente.foto,
                es_lider=False
            )

            # Tratar de convertir el Consolidador del creyente actual
            # A Líder para asignarlo como líder del nuevo Consolidador
            if creyente.consolidador.is_leader:
                consolidador.leader = consolidador

            consolidador.etapas.add(creyente.etapa)
            consolidador.estados_civiles.add(creyente.estado_civil)
            consolidador.save()

            # Marcar el Creyente que paso a ser consolidador
            # Como Consolidado
            creyente.nota += '\n' + request.user.first_name + ' ' + get_hoy() + ":\n" + \
                             "Creyente promovido a Consolidador" + '\r\n'

            creyente.status = CreyenteStatus.PROMOTED
            creyente.save()

        return HttpResponseRedirect('/detalles_consolidador/?id=%s' % consolidador.id)
    else:
        raise PermissionDenied()


@login_required(login_url='/login/?next=/xls_report/')
def exportar_creyentes_xls(request):
    if user_has_access(request.user, Access.LIDER_MINISTERIO):
        def select_format(col_number: int):
            if col_number in [6, 7, 9, 10]:
                return xlwt.easyxf(num_format_str='D-MMM-YY')
            else:
                return xlwt.XFStyle()

        def cleaner(col_name: str, col_value):
            if col_name in ('Teléfono', 'Celular', 'Otro'):
                if str(col_value) == '8090000000':
                    return ''
                else:
                    return col_value
            return col_value

        lideres = models.Consolidador.active_objects.filter(es_lider=True).values("id", "nombre")
        creyente_list = list()
        file_name = "creyentes.xls"
        aviso = ""

        if request.method == 'POST':
            desde = request.POST.get('desde')
            hasta = request.POST.get('hasta')
            lider = None

            if 'lider' in request.POST:
                lider = models.Consolidador.active_objects.get(id=int(request.POST.get('lider')), es_lider=True)
                file_name = f"creyentes_{lider.nombre.replace(' ', '')}_{datetime.today().strftime('%d%m%Y%H%M')}.xls"
                creyente_list = models.Creyente.objects.lider_network(lider) \
                    .values_list('nombre', 'telefono', 'celular', 'otro',
                                 'consolidador__nombre'
                                 'actividad__actividad', 'fecha_culto', 'fecha_creacion') \
                    .order_by('fecha_culto')

            elif hasta is not None:
                file_name = f"creyente_{datetime.today().strftime('%d%m%Y%H%M')}.xls"
                creyente_list = models.Creyente.objects.filter(fecha_culto__range=[desde, hasta])\
                    .values_list('nombre', 'telefono', 'celular', 'otro',
                                 'consolidador__nombre', 'actividad__actividad', 'fecha_culto', 'fecha_creacion')\
                    .order_by('fecha_culto')

            if creyente_list:
                response = HttpResponse(content_type='application/ms-excel')
                response['Content-Disposition'] = f'attachment; filename="{file_name}"'

                # Workbook
                wb = xlwt.Workbook(encoding='utf-8')
                sheet_info = wb.add_sheet('Creyentes', cell_overwrite_ok=True)
                # sheet_form = wb.add_sheet('Formulario', cell_overwrite_ok=True)


                # Sheet header, first row
                row_num = 0
                title_style = xlwt.XFStyle()
                title_style.font.bold = True

                info_columns = ['Nombre', 'Teléfono', 'Celular', 'Otro', 'Primer Consolidador',
                                'Segundo Consolidador', 'Próxima Llamada', 'Próxima Visita',
                                'Actividad', 'Fecha Actividad', 'Fecha Agregado']

                # form_columns = ['Nombre', 'Asiste', 'Célula', 'G12', 'Encargado', 'Universidad de La Vida',
                #                 'Encuentro', 'Capacitación Destino', 'Bautizado', 'Razón']

                # Write Titles
                # Creyentes info
                for col_num in range(len(info_columns)):
                    sheet_info.write(row_num, col_num, info_columns[col_num], title_style)
                # Form
                # for col_num in range(len(form_columns)):
                #     sheet_form.write(row_num, col_num, form_columns[col_num], title_style)

                # Write
                for row in creyente_list:
                    row_num += 1
                    for col_num in range(len(row)):
                        cel_value = cleaner(info_columns[col_num], row[col_num])
                        # Information values
                        sheet_info.write(row_num, col_num, cel_value, select_format(col_num))
                    # Just names on the Form sheet
                    # sheet_form.write(row_num, 0, row[0], xlwt.XFStyle())

                wb.save(response)

                return response
            else:
                if lider is None:
                    aviso = f'Desde {desde} hasta {hasta}, no hay datos'
                else:
                    aviso = f'El Líder {lider.nombre} no tiene creyentes en su red.'
                return render(request, 'reports/to_xls.html', {
                    'lideres': lideres,
                    'aviso': aviso
                })
        else:
            return render(request, 'reports/to_xls.html', {'lideres': lideres})
    else:
        raise PermissionDenied()


@login_required(login_url='/login/?next=/import_creyentes/')
def import_creyentes_xls(request):
    if user_has_access(request.user, Access.FULL):

        actividades = models.Actividad.objects.all().values("id", "actividad")

        if request.method == 'POST' and 'import-file' in request.FILES:
            file = request.FILES['import-file'].file
            sheet = xlrd.open_workbook(file).sheet_by_index(0)
            rows = sheet.nrows
        else:
            return render(request, 'reports/from_xls.html', {'actividades': actividades})
    else:
        raise PermissionDenied()
