#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import date, datetime

from django import forms
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from Funciones import get_hoy
from consolidacion import models
from consolidacion.decorators import can_edit_person_info
from customs.fields import CustomPhoneField, CustomEmailField, CustomMultipleChoice, CustomSingleChoice


year = date.today().year
INVALID_CHARACTERS_REGEX = r"!@#$%^&*()-+_"
TEL_ATTRS = {'class': 'form-control',
                      'type': "tel",
                      'id': "example-tel-input",
                      'pattern': r"\d{10}",
                      'placeholder': 'formato: 8092221111 o vacío'
                      }


class AddCreyente(forms.ModelForm):
    class Meta:
        model = models.Creyente
        fields = ['fecha_culto', 'foto', 'nombre', 'edad', 'apodo',
                  'direccion', 'telefono', 'celular', 'otro', 'sexo', 'estado_civil',
                  'actividad', 'email', 'estado_laboral', 'ocupacion', 'reunion', 'nota',
                  'bautizado', 'encuentro', 'reencuentro', 'escuela_lideres']

    error_css_class = 'alert alert-danger h6'
    nombre = forms.CharField(max_length=30)
    apodo = forms.CharField(max_length=15, required=False)
    direccion = forms.CharField(max_length=200, label='Dirección')
    foto = forms.ImageField(required=False)
    edad = forms.IntegerField(widget=forms.TextInput(attrs={'type': 'number'}))
    telefono = CustomPhoneField(label='Teléfono')
    celular = CustomPhoneField()
    otro = CustomPhoneField()
    email = CustomEmailField()
    estado_laboral = CustomMultipleChoice(queryset=models.Laboral.objects.all())
    ocupacion = CustomMultipleChoice(queryset=models.Ocupacion.objects.all(), label='Ocupación')
    estado_civil = CustomSingleChoice(queryset=models.Civil.objects.all(), required=True)
    sexo = CustomSingleChoice(queryset=models.Sexo.objects.all(), required=True)

    fecha_culto = forms.DateField(label='Fecha Actividad', widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'date',
        'value': datetime.strptime(get_hoy(), "%d/%m/%Y").strftime("%Y-%m-%d"),
        'id': 'example-date-input'
    }), required=True)
    actividad = CustomSingleChoice(queryset=models.Actividad.objects.all(), initial=2)
    reunion = forms.CharField(max_length=200, required=False, widget=forms.Textarea(attrs={
        'class': 'form-control'
    }))

    nota = forms.CharField(max_length=200, required=False, widget=forms.Textarea(attrs={
        'class': 'form-control'
    }))

    bautizado = forms.CheckboxInput()
    encuentro = forms.CheckboxInput()
    reencuentro = forms.CheckboxInput()
    escuela_lideres = forms.CheckboxInput()

    def clean_nombre(self):
        # remove any whitespace from the end of the string
        # and make it capitalized
        nombre = self.cleaned_data['nombre'].strip().title()

        # Validate there is not dots at the end
        if nombre[-1] == ".":
            nombre = nombre[:-1]

        # just to make sure
        nombre = nombre.strip().title()

        # Validate there are no special characters on the name
        if any(char in INVALID_CHARACTERS_REGEX for char in nombre):
            raise forms.ValidationError("Caracteres Inválidos para un nombre: ({})".format(INVALID_CHARACTERS_REGEX))

        # In case there was more than one dot a the end
        if nombre[-1] == ".":
            raise forms.ValidationError("El nombre no puede terminar con punto")

        # Validate the 'nombre' value is unique
        # if models.Creyente.objects.filter(nombre=nombre).first() is not None:
        #     raise forms.ValidationError("Ya existe un Creyente con este nombre: {}".format(nombre))

        return nombre


class AddNoCreyente(forms.ModelForm):

    class Meta:
        model = models.Creyente
        fields = ['foto', 'nombre', 'edad', 'apodo',
                  'direccion', 'telefono', 'celular', 'otro', 'sexo', 'estado_civil',
                  'email', 'estado_laboral', 'ocupacion', 'nota']
    error_css_class = 'alert alert-danger'

    nombre = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    apodo = forms.CharField(max_length=15, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    direccion = forms.CharField(max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))

    foto = forms.ImageField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'file'}), required=False)

    edad = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': "number"}))

    telefono = forms.CharField(max_length=10, required=False, widget=forms.TextInput(attrs=TEL_ATTRS))
    celular = forms.CharField(max_length=10, required=False, widget=forms.TextInput(attrs=TEL_ATTRS))
    otro = forms.CharField(max_length=10, required=False, widget=forms.TextInput(attrs=TEL_ATTRS))
    email = forms.EmailField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control',
               'type': "email",
               'id': "example-email-input",
               'pattern': r"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$",
               'placeholder': 'formato: ejemplo@example.com'
               }))

    estado_laboral = forms.ModelMultipleChoiceField(queryset=models.Laboral.objects.all(), required=False,
                                                    widget=forms.SelectMultiple(
        attrs={'class': 'form-control', }
    ))
    ocupacion = forms.ModelMultipleChoiceField(queryset=models.Ocupacion.objects.all(), required=False,
                                               widget=forms.SelectMultiple(
        attrs={'class': 'form-control', }
    ))
    estado_civil = forms.ModelChoiceField(queryset=models.Civil.objects.all(), required=True, widget=forms.Select(
        attrs={'class': 'form-control mb-2 mr-sm-2 mb-sm-0', }
    ))

    sexo = forms.ModelChoiceField(queryset=models.Sexo.objects.all(), required=True, widget=forms.Select(
        attrs={'class': 'form-control mb-2 mr-sm-2 mb-sm-0', }
    ))

    nota = forms.CharField(max_length=200, required=False, widget=forms.Textarea(attrs={
        'class': 'form-control'
    }))

    def clean_nombre(self):
        # remove any whitespace from the end of the string
        # and make it capitalized
        nombre = self.cleaned_data['nombre'].strip().title()

        # Validate there is not dots at the end
        if nombre[-1] == ".":
            nombre = nombre[:-1]

        # just to make sure
        nombre = nombre.strip().title()

        # Validate there are no special characters on the name
        if any(char in INVALID_CHARACTERS_REGEX for char in nombre):
            raise forms.ValidationError("Caracteres Inválidos para un nombre: ({})".format(INVALID_CHARACTERS_REGEX))

        # In case there was more than one dot a the end
        if nombre[-1] == ".":
            raise forms.ValidationError("El nombre no puede terminar con punto")

        # Validate the 'nombre' value is unique
        if models.Creyente.objects.filter(nombre=nombre).first() is not None:
            raise forms.ValidationError("Ya existe un Creyente con este nombre: {}".format(nombre))


class AddCelula(forms.ModelForm):
    class Meta:
        model = models.Celula
        fields = ['consolidador', 'ubicacion', 'dia', 'hora', 'frecuencia', 'prox_reporte', 'nota']

    error_css_class = 'alert alert-danger'

    consolidador = forms.ModelChoiceField(queryset=models.Consolidador.active_objects.all())
    ubicacion = forms.CharField(max_length=200, label='Ubicación',
                                widget=forms.TextInput(attrs={'class': 'form-control'}))
    dia = CustomSingleChoice(label='Día', queryset=models.Dia.objects.all())
    hora = forms.TimeField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'time',
        'id': 'example-time-input'
    }), required=True)
    frecuencia = forms.ModelChoiceField(queryset=models.Frecuencia.objects.all())
    prox_reporte = forms.DateField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'date',
        'value': datetime.strptime(get_hoy(), "%d/%m/%Y").strftime("%Y-%m-%d"),
        'id': 'example-date-input'
    }), required=True)

    nota = forms.CharField(max_length=200, required=False, widget=forms.Textarea(attrs={
        'class': 'form-control'
    }))


class AddConsolidador(forms.ModelForm):

    class Meta:
        model = models.Consolidador
        fields = ['es_lider', 'nombre', 'sexo', 'foto',
                  'telefono', 'cel', 'otro', 'email', 'leader', 'etapas', 'estados_civiles']
    error_css_class = 'alert alert-danger'
    es_lider = forms.BooleanField(required=False)
    nombre = forms.CharField(max_length=30, widget=forms.TextInput())
    sexo = forms.ModelChoiceField(queryset=models.Sexo.objects.all(), required=True, widget=forms.Select())
    foto = forms.ImageField(required=False)
    telefono = CustomPhoneField(label='Teléfono')
    cel = CustomPhoneField(label='Celular')
    otro = CustomPhoneField()
    email = CustomEmailField()
    leader = forms.ModelChoiceField(label='Líder', required=False,
                                    queryset=models.Consolidador.active_objects.filter(es_lider=True).all())
    etapas = CustomMultipleChoice(queryset=models.Etapa.objects.all())
    estados_civiles = CustomMultipleChoice(queryset=models.Civil.objects.all())

    def clean_nombre(self):
        # remove any whitespace from the end of the string
        # and make it capitalized
        nombre = self.cleaned_data['nombre'].strip().title()

        # Validate there is not dots at the end
        if nombre[-1] == ".":
            nombre = nombre[:-1]

        # just to make sure
        nombre = nombre.strip().title()

        # Validate there are no special characters on the name
        if any(char in INVALID_CHARACTERS_REGEX for char in nombre):
            raise forms.ValidationError("Caracteres Inválidos para un nombre: ({})".format(INVALID_CHARACTERS_REGEX))

        # In case there was more than one dot a the end
        if nombre[-1] == ".":
            raise forms.ValidationError("El nombre no puede terminar con punto")

        # Validate the 'nombre' value is unique
        if models.Consolidador.active_objects.filter(nombre=nombre).first() is not None:
            raise forms.ValidationError("Ya existe un Consolidador con este nombre: {}".format(nombre))

        return nombre

    def clean_leader(self):
        leader = self.cleaned_data['leader']
        if leader == self.cleaned_data['nombre']:
            raise forms.ValidationError('No puede elegirse a sí mismo como lider')

        return leader


@method_decorator(can_edit_person_info, name='dispatch')
class ActualizarConsolidador(UpdateView):
    model = models.Consolidador
    fields = ['foto', 'nombre', 'etapas',
              'telefono', 'cel', 'otro', 'estados_civiles', 'leader', 'email']


@method_decorator(can_edit_person_info, name='dispatch')
class ActualizarCreyente(UpdateView):
    model = models.Creyente
    fields = ['nombre', 'foto', 'direccion', 'apodo', 'telefono',
              'celular', 'otro', 'email', 'estado_civil',
              'actividad', 'estado_laboral', 'ocupacion', 'reunion',
              'bautizado', 'encuentro', 'reencuentro', 'escuela_lideres']


class AddOcupacion(CreateView):
    model = models.Ocupacion
    fields = ['ocupacion']
    template_name = 'form_add.html'


class AddFrecuencia(CreateView):
    model = models.Frecuencia
    fields = ['frecuencia']
    template_name = 'form_add.html'


class ActualizarOcupacion(UpdateView):
    model = models.Ocupacion
    fields = ['ocupacion']


class AddActividad(CreateView):
    model = models.Actividad
    fields = ['actividad']
    template_name = 'form_add.html'


class ActualizarActividad(UpdateView):
    model = models.Actividad
    fields = ['actividad']


class DarBaja(forms.Form):
    razon = forms.ModelChoiceField(queryset=models.Razon.objects.all(), required=True, empty_label="--")
    detalle = forms.CharField(max_length=1000, widget=forms.TextInput(attrs={'class': 'form-control'}))
