from django.core.exceptions import PermissionDenied
from consolidacion.models import Creyente, Consolidador
from consolidacion.templatetags.etiquetas import check_access_level
from functools import wraps


def can_edit_person_info(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):

        logged_user = request.user

        if _can_edit_person_object(logged_user, request.resolver_match.url_name, **kwargs):
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied

    try:
        wrap.__doc__ = function.__doc__
    except AttributeError:
        pass

    try:
        wrap.__name__ = function.__name__
    except AttributeError:
        pass

    return wrap


def _can_edit_person_object(logged_user, object_name, **kwargs):
    main_access = check_access_level(logged_user, 'ministerio')

    if object_name in ['actualizar-creyente', 'edit-visit', 'edit-call', 'add-note']:
        creyente = Creyente.objects.get(pk=kwargs['pk'])

        return main_access or logged_user.first_name == creyente.consolidador.nombre or \
               creyente.consolidador.leader.nombre == logged_user.first_name

    elif object_name == 'actualizar-consolidador':
        consolidador = Consolidador.active_objects.get(pk=kwargs['pk'])

        return main_access or logged_user.first_name == consolidador.nombre or \
               consolidador.leader.nombre == logged_user.first_name

    elif object_name == '':
        return logged_user.is_superuser
