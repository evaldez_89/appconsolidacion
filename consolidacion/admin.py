#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

from consolidacion.models import *


# Register your models here.
class ConsolidadorAdmin(admin.ModelAdmin):
    fields = (('is_deleted', 'is_active', 'es_lider'),
              ('nombre', 'sexo'),
              ('telefono', 'cel', 'otro', 'email'), ('leader', 'foto'), 'etapas', 'estados_civiles',)
    list_display = ('nombre', 'telefono', 'email', 'leader')
    list_display_links = ('nombre',)
    search_fields = ('nombre', 'telefono', 'cel', 'otro', 'email', 'leader')
    list_filter = ('leader', 'is_active', 'is_deleted')
    filter_horizontal = ('etapas', 'estados_civiles')


class LiderAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'telefono', 'email')
    search_fields = ('nombre', 'telefono')


class CreyenteAdmin(admin.ModelAdmin):

    list_display = ('nombre', 'consolidador', 'telefono', 'celular')
    search_fields = ('nombre', 'telefono', 'consolidador')
    list_filter = ('consolidador', 'etapa', 'estado_laboral', 'estado_civil', 'ocupacion', 'sexo',
                   'actividad', 'fecha_culto')
    ordering = ('nombre',)

    save_on_top = True

    fields = ('status', 'fecha_culto', ('nombre', 'fecha_nacimiento', 'foto'), ('direccion', 'location', 'apodo'),
              ('telefono', 'celular'), ('otro', 'email'), ('estado_civil', 'sexo'),
              ('actividad', 'estado_laboral'),
              'ocupacion', 'nota',
              'bautizado', 'encuentro', 'reencuentro', 'escuela_lideres')

    def response_add(self, request, obj, post_url_continue="../%s/"):
        if '_continue' not in request.POST:
            return HttpResponseRedirect('/disp_consolidador/?nombre='+str(request.POST['nombre']))
        else:
            return super(CreyenteAdmin, self).response_add(request, obj, post_url_continue)


class DiaAdmin(admin.ModelAdmin):
    list_display = ('dia', 'id')


class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'last_login')


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Etapa)
admin.site.register(Laboral)
admin.site.register(Ocupacion)
admin.site.register(Civil)
admin.site.register(Sexo)
admin.site.register(Consolidador, ConsolidadorAdmin)
admin.site.register(Creyente, CreyenteAdmin)
admin.site.register(Actividad)
admin.site.register(Dia, DiaAdmin)
admin.site.register(Razon)
admin.site.register(Frecuencia)
admin.site.register(Iglesia)
