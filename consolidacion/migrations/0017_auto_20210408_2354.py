# Generated by Django 3.1.7 on 2021-04-09 03:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0016_remove_consolidador_lider'),
    ]

    operations = [
        migrations.RenameField(
            model_name='consolidador',
            old_name='leader_id',
            new_name='leader',
        ),
    ]
