# Generated by Django 2.1.5 on 2020-02-17 00:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0008_creyente_location'),
    ]

    operations = [
        migrations.RenameField(
            model_name='celula',
            old_name='ubicacion',
            new_name='address',
        ),
        migrations.AddField(
            model_name='celula',
            name='location',
            field=models.CharField(default='-69.908897,18.535497', max_length=50, verbose_name='Ubicación'),
        ),
    ]
