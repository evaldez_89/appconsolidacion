# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actividad',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('actividad', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Actividades',
            },
        ),
        migrations.CreateModel(
            name='Celula',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('ubicacion', models.TextField(blank=True, null=True)),
                ('hora', models.TimeField()),
                ('prox_reporte', models.DateField(blank=True, null=True)),
                ('nota', models.TextField(blank=True, null=True)),
                ('fecha_creacion', models.DateField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Civil',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('estado_civil', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Estados Civiles',
            },
        ),
        migrations.CreateModel(
            name='Consolidador',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('nombre', models.CharField(unique=True, max_length=30)),
                ('telefono', models.CharField(blank=True, max_length=15)),
                ('cel', models.CharField(blank=True, max_length=15, default=8090000000)),
                ('otro', models.CharField(blank=True, max_length=15, default=8090000000)),
                ('email', models.EmailField(verbose_name='e-mail', blank=True, max_length=254)),
                ('foto', models.ImageField(blank=True, upload_to='consolidadores')),
                ('es_lider', models.BooleanField()),
                ('primario', models.BooleanField(default=True)),
                ('secundario', models.BooleanField(default=True)),
                ('fecha_creacion', models.DateField(auto_now_add=True, null=True)),
                ('estados_civiles', models.ManyToManyField(to='consolidacion.Civil')),
            ],
            options={
                'verbose_name_plural': 'Consolidadores',
                'ordering': ['nombre'],
            },
        ),
        migrations.CreateModel(
            name='Creyente',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('nombre', models.CharField(verbose_name='Nombre Completo', max_length=30)),
                ('apodo', models.CharField(blank=True, max_length=15)),
                ('direccion', models.CharField(max_length=200)),
                ('cedula', models.CharField(blank=True, max_length=11)),
                ('fecha_nacimiento', models.DateField(blank=True, null=True)),
                ('foto', models.ImageField(blank=True, upload_to='creyentes')),
                ('edad', models.IntegerField()),
                ('telefono', models.CharField(blank=True, max_length=10, default=8090000000)),
                ('celular', models.CharField(blank=True, max_length=10, default=8090000000)),
                ('otro', models.CharField(blank=True, max_length=10)),
                ('email', models.EmailField(verbose_name='e-mail', blank=True, max_length=254)),
                ('fecha_culto', models.DateField(blank=True, null=True)),
                ('reunion', models.TextField(blank=True, null=True)),
                ('nota', models.TextField(blank=True, null=True)),
                ('bautizado', models.BooleanField(default=False)),
                ('encuentro', models.BooleanField(default=False)),
                ('reencuentro', models.BooleanField(default=False)),
                ('escuela_lideres', models.BooleanField(default=False)),
                ('prox_llamada', models.DateField(blank=True, null=True)),
                ('prox_visita', models.DateField(blank=True, null=True)),
                ('fecha_creacion', models.DateField(auto_now_add=True, null=True)),
                ('actividad', models.ForeignKey(to='consolidacion.Actividad', default=11, on_delete=models.SET_DEFAULT)),
                ('celula', models.ForeignKey(related_name='celula', to='consolidacion.Celula', null=True, blank=True,
                                             on_delete=models.SET_DEFAULT, default=None,)),
                ('consolidador', models.ForeignKey(related_name='creyente_consolidador', null=True, blank=True,
                                                   on_delete=models.SET_DEFAULT, to='consolidacion.Consolidador')),
                ('consolidador_dos', models.ForeignKey(related_name='creyente_consolidador_dos', null=True, blank=True,
                                                       on_delete=models.SET_DEFAULT, to='consolidacion.Consolidador')),
            ],
        ),
        migrations.CreateModel(
            name='Dia',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('dia', models.CharField(unique=True, max_length=12)),
            ],
        ),
        migrations.CreateModel(
            name='Etapa',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('etapa', models.CharField(unique=True, max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Frecuencia',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('frecuencia', models.CharField(unique=True, max_length=15)),
            ],
        ),
        migrations.CreateModel(
            name='Invitador',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('invitador', models.CharField(unique=True, max_length=20)),
                ('foto', models.ImageField(blank=True, upload_to='invitadores')),
                ('fecha_creacion', models.DateField(auto_now_add=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Invitadores',
            },
        ),
        migrations.CreateModel(
            name='Laboral',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('estado_laboral', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Estados Laborales',
            },
        ),
        migrations.CreateModel(
            name='Lider',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('nombre', models.CharField(unique=True, max_length=30)),
                ('telefono', models.CharField(max_length=15, default='8090000000')),
                ('cel', models.CharField(blank=True, max_length=15, default='8090000000')),
                ('otro', models.CharField(blank=True, max_length=15, default='8090000000')),
                ('email', models.EmailField(verbose_name='e-mail', blank=True, max_length=254)),
                ('foto', models.ImageField(blank=True, upload_to='lideres')),
                ('fecha_creacion', models.DateField(auto_now_add=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Lideres',
            },
        ),
        migrations.CreateModel(
            name='Ocupacion',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('ocupacion', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Ocupaciones',
            },
        ),
        migrations.CreateModel(
            name='Razon',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('razon', models.CharField(unique=True, max_length=25)),
            ],
            options={
                'verbose_name_plural': 'Razones',
            },
        ),
        migrations.CreateModel(
            name='Sexo',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('sexo', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Sexos (M/F)',
            },
        ),
        migrations.CreateModel(
            name='Trayectoria',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('proceso', models.CharField(unique=True, max_length=20)),
                ('completado', models.BooleanField(default=False)),
            ],
        ),
        migrations.AddField(
            model_name='creyente',
            name='dia',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Dia', on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_civil',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Civil', on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ManyToManyField(blank=True, to='consolidacion.Laboral'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='etapa',
            field=models.ForeignKey(to='consolidacion.Etapa', on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='creyente',
            name='invitado_por',
            field=models.ManyToManyField(blank=True, to='consolidacion.Invitador'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='ocupacion',
            field=models.ManyToManyField(blank=True, to='consolidacion.Ocupacion'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='razon',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Razon', on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='creyente',
            name='sexo',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Sexo', on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='etapas',
            field=models.ManyToManyField(to='consolidacion.Etapa'),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='lider',
            field=models.ForeignKey(to='consolidacion.Lider', default=5, on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='sexo',
            field=models.ForeignKey(to='consolidacion.Sexo', on_delete=models.SET_DEFAULT),
        ),
        migrations.AddField(
            model_name='celula',
            name='consolidador',
            field=models.ForeignKey(related_name='lider_celula', null=True, blank=True,
                                    on_delete=models.SET_DEFAULT, to='consolidacion.Consolidador'),
        ),
        migrations.AddField(
            model_name='celula',
            name='dia',
            field=models.ForeignKey(related_name='dia_de_reunion', to='consolidacion.Dia', null=True, blank=True,
                                    on_delete=models.SET_DEFAULT, default=None),
        ),
        migrations.AddField(
            model_name='celula',
            name='frecuencia',
            field=models.ForeignKey(to='consolidacion.Frecuencia', null=True, blank=True,
                                    on_delete=models.SET_DEFAULT, default=None),
        ),
        migrations.AlterUniqueTogether(
            name='creyente',
            unique_together=set([('nombre', 'telefono', 'celular')]),
        ),
    ]
