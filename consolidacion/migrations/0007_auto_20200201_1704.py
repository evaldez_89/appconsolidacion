# Generated by Django 2.1.5 on 2020-02-01 21:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0006_auto_20200201_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consolidador',
            name='es_lider',
            field=models.BooleanField(blank=True),
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='primario',
            field=models.BooleanField(blank=True, default=True),
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='secundario',
            field=models.BooleanField(blank=True, default=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='bautizado',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='encuentro',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='escuela_lideres',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='reencuentro',
            field=models.BooleanField(blank=True, default=False),
        ),
        migrations.AlterField(
            model_name='trayectoria',
            name='completado',
            field=models.BooleanField(blank=True, default=False),
        ),
    ]
