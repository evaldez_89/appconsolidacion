# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0041_auto_20150902_1915'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consolidador',
            name='consolidando',
        ),
        migrations.RemoveField(
            model_name='lider',
            name='discipulos',
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='lider',
            field=models.ForeignKey(to='consolidacion.Lider', default=5),
        ),
    ]
