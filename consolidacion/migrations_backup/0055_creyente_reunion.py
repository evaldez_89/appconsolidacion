# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0054_auto_20160930_0002'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='reunion',
            field=models.TextField(blank=True, null=True),
        ),
    ]
