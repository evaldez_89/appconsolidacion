# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0005_auto_20150517_2244'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='edad',
            field=models.IntegerField(default=0, max_length=2),
            preserve_default=False,
        ),
    ]
