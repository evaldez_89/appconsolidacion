# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0044_auto_20160926_2310'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='celular',
            field=models.CharField(max_length=10, blank=True),
        ),
        migrations.RemoveField(
            model_name='creyente',
            name='estado_laboral',
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ForeignKey(to='consolidacion.Laboral', null=True, blank=True),
        ),
        migrations.RemoveField(
            model_name='creyente',
            name='ocupacion',
        ),
        migrations.AddField(
            model_name='creyente',
            name='ocupacion',
            field=models.ForeignKey(to='consolidacion.Ocupacion', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='telefono',
            field=models.CharField(max_length=10, blank=True),
        ),
    ]
