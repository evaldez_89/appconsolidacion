# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0050_auto_20160929_2026'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trayecto',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('bautizado', models.BooleanField(default=False)),
                ('encuentro', models.BooleanField(default=False)),
                ('reencuentro', models.BooleanField(default=False)),
                ('escuela_lideres', models.BooleanField(default=False)),
            ],
        ),
        migrations.AddField(
            model_name='creyente',
            name='trayecto',
            field=models.ForeignKey(blank=True, to='consolidacion.Trayecto', null=True),
        ),
    ]
