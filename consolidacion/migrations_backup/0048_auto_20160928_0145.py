# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0047_auto_20160928_0051'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='estado_civil',
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_civil',
            field=models.ForeignKey(blank=True, to='consolidacion.Civil', null=True),
        ),
        migrations.RemoveField(
            model_name='creyente',
            name='sexo',
        ),
        migrations.AddField(
            model_name='creyente',
            name='sexo',
            field=models.ForeignKey(blank=True, to='consolidacion.Sexo', null=True),
        ),
    ]
