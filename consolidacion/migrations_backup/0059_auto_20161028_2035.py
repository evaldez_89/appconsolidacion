# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0058_auto_20161010_2113'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='creyente',
            options={},
        ),
    ]
