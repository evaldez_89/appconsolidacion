# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0006_creyente_edad'),
    ]

    operations = [
        migrations.AddField(
            model_name='consolidador',
            name='consolidadondo',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='edad',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='invitado_por',
            field=models.ManyToManyField(to='consolidacion.Invitador', blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='nombre',
            field=models.CharField(max_length=30, verbose_name=b'Nombre Completo'),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='otro',
            field=models.CharField(max_length=10, blank=True),
        ),
    ]
