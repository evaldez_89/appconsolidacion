# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0042_auto_20160926_2235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consolidador',
            name='cel',
            field=models.CharField(max_length=15, default=8090000000, blank=True),
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='otro',
            field=models.CharField(max_length=15, default=8090000000, blank=True),
        ),
    ]
