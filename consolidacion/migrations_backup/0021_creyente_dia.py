# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0020_creyente_actividad'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='dia',
            field=models.ForeignKey(blank=True, to='consolidacion.Dia', null=True),
        ),
    ]
