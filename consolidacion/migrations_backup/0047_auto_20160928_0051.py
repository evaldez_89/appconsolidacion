# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0046_auto_20160928_0037'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='estado_civil',
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_civil',
            field=models.ManyToManyField(to='consolidacion.Civil'),
        ),
    ]
