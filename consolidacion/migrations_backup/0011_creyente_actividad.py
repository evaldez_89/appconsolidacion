# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0010_auto_20150810_1747'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='actividad',
            field=models.ForeignKey(default=1, to='consolidacion.Actividad'),
            preserve_default=False,
        ),
    ]
