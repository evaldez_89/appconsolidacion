# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0049_auto_20160929_2025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ManyToManyField(to='consolidacion.Laboral', blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='ocupacion',
            field=models.ManyToManyField(to='consolidacion.Ocupacion', blank=True),
        ),
    ]
