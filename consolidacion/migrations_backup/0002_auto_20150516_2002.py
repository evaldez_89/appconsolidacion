# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='civil',
            options={'verbose_name_plural': 'Estados Civiles'},
        ),
        migrations.AlterModelOptions(
            name='consolidador',
            options={'verbose_name_plural': 'Consolidadores'},
        ),
        migrations.AlterModelOptions(
            name='invitador',
            options={'verbose_name_plural': 'Invitadores'},
        ),
        migrations.AlterModelOptions(
            name='laboral',
            options={'verbose_name_plural': 'Estados Laborales'},
        ),
        migrations.AlterModelOptions(
            name='ocupacion',
            options={'verbose_name_plural': 'Ocupaciones'},
        ),
        migrations.AlterModelOptions(
            name='sexo',
            options={'verbose_name_plural': 'Sexos (M/F)'},
        ),
        migrations.AlterField(
            model_name='creyente',
            name='email',
            field=models.EmailField(blank=True, verbose_name='e-mail', max_length=254),
        ),
    ]
