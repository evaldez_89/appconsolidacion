# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0003_consolidador_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='prox_llamada',
            field=models.CharField(max_length=30, default='18-05-2015 04:20 PM'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='prox_visita',
            field=models.CharField(max_length=30, default='18-05-2015 10:20 PM'),
        ),
    ]
