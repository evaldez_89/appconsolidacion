# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0055_creyente_reunion'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='consolidador_dos',
            field=models.ForeignKey(related_name='creyente_consolidador_dos', null=True, blank=True, to='consolidacion.Consolidador'),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='consolidador',
            field=models.ForeignKey(related_name='creyente_consolidador', null=True, blank=True, to='consolidacion.Consolidador'),
        ),
    ]
