# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0064_auto_20170908_2330'),
    ]

    operations = [
        migrations.AddField(
            model_name='celula',
            name='fecha_creacion',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='fecha_creacion',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='creyente',
            name='fecha_creacion',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='invitador',
            name='fecha_creacion',
            field=models.DateField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='lider',
            name='fecha_creacion',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
