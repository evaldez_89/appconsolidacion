# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0036_auto_20150826_1045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='consolidador',
            field=models.ForeignKey(to='consolidacion.Consolidador', null=True),
        ),
    ]
