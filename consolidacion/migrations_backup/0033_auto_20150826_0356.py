# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0032_auto_20150819_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='consolidador',
            field=models.ForeignKey(blank=True, to='consolidacion.Consolidador'),
        ),
    ]
