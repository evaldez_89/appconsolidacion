# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0008_auto_20150804_1457'),
    ]

    operations = [
        migrations.CreateModel(
            name='Actividad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('actividad', models.CharField(max_length=20)),
            ],
        ),
        migrations.AlterField(
            model_name='creyente',
            name='cedula',
            field=models.CharField(unique=True, max_length=11, blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='email',
            field=models.EmailField(unique=True, max_length=254, verbose_name=b'e-mail', blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='nombre',
            field=models.CharField(unique=True, max_length=30, verbose_name=b'Nombre Completo'),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='nota',
            field=models.TextField(null=True, blank=True),
        ),
    ]
