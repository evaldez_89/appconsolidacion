# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0013_auto_20150810_1842'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dias',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dias', models.CharField(max_length=12)),
            ],
        ),
        migrations.AddField(
            model_name='actividad',
            name='dias',
            field=models.ForeignKey(default=1, to='consolidacion.Dias'),
            preserve_default=False,
        ),
    ]
