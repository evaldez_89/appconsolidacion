# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0004_auto_20150517_2220'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='estado_laboral',
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ManyToManyField(to='consolidacion.Laboral'),
        ),
        migrations.RemoveField(
            model_name='creyente',
            name='invitado_por',
        ),
        migrations.AddField(
            model_name='creyente',
            name='invitado_por',
            field=models.ManyToManyField(to='consolidacion.Invitador'),
        ),
        migrations.RemoveField(
            model_name='creyente',
            name='ocupacion',
        ),
        migrations.AddField(
            model_name='creyente',
            name='ocupacion',
            field=models.ManyToManyField(to='consolidacion.Ocupacion'),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='prox_llamada',
            field=models.CharField(max_length=30),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='prox_visita',
            field=models.CharField(max_length=30),
        ),
    ]
