# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0039_auto_20150828_0326'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='consolidador',
            options={'ordering': ['nombre'], 'verbose_name_plural': 'Consolidadores'},
        ),
        migrations.AlterModelOptions(
            name='creyente',
            options={'ordering': ['prox_llamada', 'prox_visita']},
        ),
        migrations.AlterField(
            model_name='creyente',
            name='fecha_nacimiento',
            field=models.DateField(),
        ),
    ]
