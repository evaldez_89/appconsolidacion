# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0012_auto_20150810_1804'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=30)),
                ('telefono', models.CharField(max_length=15)),
                ('email', models.EmailField(max_length=254, verbose_name=b'e-mail', blank=True)),
                ('discipulos', models.IntegerField(default=0)),
            ],
            options={
                'verbose_name_plural': 'Lideres',
            },
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='nombre',
            field=models.CharField(unique=True, max_length=30),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='lider',
            field=models.ForeignKey(default=1, to='consolidacion.Lider'),
            preserve_default=False,
        ),
    ]
