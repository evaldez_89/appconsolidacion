# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0030_auto_20150819_1302'),
    ]

    operations = [
        migrations.RenameField(
            model_name='consolidador',
            old_name='estados_civils',
            new_name='estados_civiles',
        ),
    ]
