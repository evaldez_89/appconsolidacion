# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0063_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Celula',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('ubicacion', models.TextField(null=True, blank=True)),
                ('hora', models.TimeField()),
                ('prox_reporte', models.DateField(null=True, blank=True)),
                ('nota', models.TextField(null=True, blank=True)),
                ('consolidador', models.ForeignKey(null=True, blank=True, related_name='lider_celula', to='consolidacion.Consolidador')),
                ('dia', models.ForeignKey(null=True, blank=True, related_name='dia_de_reunion', to='consolidacion.Dia', default=None)),
            ],
        ),
        migrations.CreateModel(
            name='Frecuencia',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('frecuencia', models.CharField(max_length=15, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Razon',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('razon', models.CharField(max_length=25, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Razones',
            },
        ),
        migrations.AlterField(
            model_name='creyente',
            name='actividad',
            field=models.ForeignKey(to='consolidacion.Actividad', default=11),
        ),
        migrations.AddField(
            model_name='celula',
            name='frecuencia',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Frecuencia', default=None),
        ),
        migrations.AddField(
            model_name='creyente',
            name='celula',
            field=models.ForeignKey(null=True, blank=True, related_name='celula', to='consolidacion.Celula', default=None),
        ),
        migrations.AddField(
            model_name='creyente',
            name='razon',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Razon'),
        ),
    ]
