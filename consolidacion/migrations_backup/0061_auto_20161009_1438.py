# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0060_auto_20161009_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='direccion',
            field=models.CharField(max_length=200),
        ),
    ]
