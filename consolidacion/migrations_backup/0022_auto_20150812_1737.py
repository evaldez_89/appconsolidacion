# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0021_creyente_dia'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='email',
            field=models.EmailField(blank=True, max_length=254, verbose_name='e-mail', unique=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='nombre',
            field=models.CharField(unique=True, max_length=30, verbose_name='Nombre Completo'),
        ),
        migrations.AlterField(
            model_name='lider',
            name='email',
            field=models.EmailField(blank=True, max_length=254, verbose_name='e-mail'),
        ),
    ]
