# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0019_remove_creyente_actividad'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='actividad',
            field=models.ForeignKey(blank=True, to='consolidacion.Actividad', null=True),
        ),
    ]
