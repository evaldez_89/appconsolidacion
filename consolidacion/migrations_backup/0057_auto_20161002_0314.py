# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0056_auto_20161002_0026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lider',
            name='cel',
            field=models.CharField(blank=True, max_length=15, default='8090000000'),
        ),
        migrations.AlterField(
            model_name='lider',
            name='otro',
            field=models.CharField(blank=True, max_length=15, default='8090000000'),
        ),
        migrations.AlterField(
            model_name='lider',
            name='telefono',
            field=models.CharField(max_length=15, default='8090000000'),
        ),
    ]
