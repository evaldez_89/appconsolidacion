# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0025_auto_20150817_0247'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='actividad',
            field=models.ForeignKey(to='consolidacion.Actividad', default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='creyente',
            name='cedula',
            field=models.CharField(blank=True, max_length=11),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='email',
            field=models.EmailField(verbose_name='e-mail', blank=True, max_length=254),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ManyToManyField(to='consolidacion.Laboral', blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='etapa',
            field=models.ForeignKey(to='consolidacion.Etapa', default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='creyente',
            name='ocupacion',
            field=models.ManyToManyField(to='consolidacion.Ocupacion', blank=True),
        ),
    ]
