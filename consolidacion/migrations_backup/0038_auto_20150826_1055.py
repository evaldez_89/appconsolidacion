# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0037_auto_20150826_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='consolidador',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Consolidador'),
        ),
    ]
