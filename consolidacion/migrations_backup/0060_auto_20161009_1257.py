# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0059_auto_20161009_1256'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='creyente',
            options={'ordering': ['-prox_llamada', '-prox_visita']},
        ),
    ]
