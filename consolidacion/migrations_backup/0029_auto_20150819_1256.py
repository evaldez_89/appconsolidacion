# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0028_auto_20150818_2253'),
    ]

    operations = [
        migrations.AddField(
            model_name='consolidador',
            name='eslider',
            field=models.CharField(default='No', choices=[('Si', 'No')], max_length=2),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='estado_civil',
            field=models.ManyToManyField(to='consolidacion.Civil'),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='sexo',
            field=models.ForeignKey(to='consolidacion.Sexo', default=1),
            preserve_default=False,
        ),
    ]
