# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0057_auto_20161002_0314'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='creyente',
            options={'ordering': ['-prox_llamada', '-prox_visita']},
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='primario',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='secundario',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='telefono',
            field=models.CharField(blank=True, max_length=15),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='celular',
            field=models.CharField(blank=True, default=8090000000, max_length=10),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='telefono',
            field=models.CharField(blank=True, default=8090000000, max_length=10),
        ),
    ]
