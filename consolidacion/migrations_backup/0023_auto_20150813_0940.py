# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models, migrations
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0022_auto_20150812_1737'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='fecha_culto',
            field=models.DateField(default=datetime.datetime(2015, 8, 13, 13, 40, 30, 172236, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='creyente',
            name='fecha_nacimiento',
            field=models.DateField(default=datetime.datetime(2015, 8, 13, 13, 40, 47, 149665, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
