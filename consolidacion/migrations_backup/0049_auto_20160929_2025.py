# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0048_auto_20160928_0145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='estado_laboral',
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ManyToManyField(null=True, to='consolidacion.Laboral', blank=True),
        ),
        migrations.RemoveField(
            model_name='creyente',
            name='ocupacion',
        ),
        migrations.AddField(
            model_name='creyente',
            name='ocupacion',
            field=models.ManyToManyField(null=True, to='consolidacion.Ocupacion', blank=True),
        ),
    ]
