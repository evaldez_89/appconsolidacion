# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0016_creyente_dia'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='actividad',
            name='dia',
        ),
    ]
