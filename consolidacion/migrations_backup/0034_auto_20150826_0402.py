# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0033_auto_20150826_0356'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='consolidador',
            field=models.CharField(max_length=20, blank=True),
        ),
    ]
