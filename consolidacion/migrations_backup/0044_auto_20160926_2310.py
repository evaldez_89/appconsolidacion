# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0043_auto_20160926_2240'),
    ]

    operations = [
        migrations.AddField(
            model_name='lider',
            name='cel',
            field=models.CharField(blank=True, max_length=15),
        ),
        migrations.AddField(
            model_name='lider',
            name='otro',
            field=models.CharField(blank=True, max_length=15),
        ),
    ]
