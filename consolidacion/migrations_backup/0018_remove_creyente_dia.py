# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0017_remove_actividad_dia'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='dia',
        ),
    ]
