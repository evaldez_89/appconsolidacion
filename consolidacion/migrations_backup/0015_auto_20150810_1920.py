# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0014_auto_20150810_1915'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Dias',
            new_name='Dia',
        ),
        migrations.RenameField(
            model_name='actividad',
            old_name='dias',
            new_name='dia',
        ),
        migrations.RenameField(
            model_name='dia',
            old_name='dias',
            new_name='dia',
        ),
    ]
