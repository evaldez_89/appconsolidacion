# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0029_auto_20150819_1256'),
    ]

    operations = [
        migrations.RenameField(
            model_name='consolidador',
            old_name='estado_civil',
            new_name='estados_civils',
        ),
    ]
