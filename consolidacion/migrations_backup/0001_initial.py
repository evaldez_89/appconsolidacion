# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actividad',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('actividad', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Actividades',
            },
        ),
        migrations.CreateModel(
            name='Civil',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('estado_civil', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Estados Civiles',
            },
        ),
        migrations.CreateModel(
            name='Consolidador',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=30)),
                ('telefono', models.CharField(max_length=15)),
                ('cel', models.CharField(default=8090000000, max_length=15)),
                ('otro', models.CharField(default=8090000000, max_length=15)),
                ('email', models.EmailField(verbose_name='e-mail', blank=True, max_length=254)),
                ('foto', models.ImageField(blank=True, upload_to='consolidadores')),
                ('es_lider', models.BooleanField()),
                ('primario', models.BooleanField(default=False)),
                ('secundario', models.BooleanField(default=False)),
                ('estados_civiles', models.ManyToManyField(to='consolidacion.Civil')),
            ],
            options={
                'verbose_name_plural': 'Consolidadores',
                'ordering': ['nombre'],
            },
        ),
        migrations.CreateModel(
            name='Creyente',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('nombre', models.CharField(unique=True, verbose_name='Nombre Completo', max_length=30)),
                ('apodo', models.CharField(blank=True, max_length=15)),
                ('direccion', models.CharField(max_length=50)),
                ('cedula', models.CharField(blank=True, max_length=11)),
                ('fecha_nacimiento', models.DateField(null=True, blank=True)),
                ('foto', models.ImageField(blank=True, upload_to='creyentes')),
                ('edad', models.IntegerField()),
                ('telefono', models.CharField(max_length=10)),
                ('celular', models.CharField(max_length=10)),
                ('otro', models.CharField(blank=True, max_length=10)),
                ('email', models.EmailField(verbose_name='e-mail', blank=True, max_length=254)),
                ('fecha_culto', models.DateField(null=True, blank=True)),
                ('nota', models.TextField(null=True, blank=True)),
                ('prox_llamada', models.CharField(max_length=30)),
                ('prox_visita', models.CharField(max_length=30)),
                ('actividad', models.ForeignKey(to='consolidacion.Actividad')),
                ('consolidador', models.ForeignKey(blank=True, null=True, to='consolidacion.Consolidador')),
            ],
            options={
                'ordering': ['prox_llamada', 'prox_visita'],
            },
        ),
        migrations.CreateModel(
            name='Dia',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('dia', models.CharField(unique=True, max_length=12)),
            ],
        ),
        migrations.CreateModel(
            name='Etapa',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('etapa', models.CharField(unique=True, max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Invitador',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('invitador', models.CharField(unique=True, max_length=20)),
                ('foto', models.ImageField(blank=True, upload_to='invitadores')),
            ],
            options={
                'verbose_name_plural': 'Invitadores',
            },
        ),
        migrations.CreateModel(
            name='Laboral',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('estado_laboral', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Estados Laborales',
            },
        ),
        migrations.CreateModel(
            name='Lider',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('nombre', models.CharField(unique=True, max_length=30)),
                ('telefono', models.CharField(max_length=15)),
                ('email', models.EmailField(verbose_name='e-mail', blank=True, max_length=254)),
                ('foto', models.ImageField(blank=True, upload_to='lideres')),
            ],
            options={
                'verbose_name_plural': 'Lideres',
            },
        ),
        migrations.CreateModel(
            name='Ocupacion',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('ocupacion', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Ocupaciones',
            },
        ),
        migrations.CreateModel(
            name='Sexo',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('sexo', models.CharField(unique=True, max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Sexos (M/F)',
            },
        ),
        migrations.AddField(
            model_name='creyente',
            name='dia',
            field=models.ForeignKey(blank=True, null=True, to='consolidacion.Dia'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_civil',
            field=models.ForeignKey(to='consolidacion.Civil'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='estado_laboral',
            field=models.ManyToManyField(to='consolidacion.Laboral', blank=True),
        ),
        migrations.AddField(
            model_name='creyente',
            name='etapa',
            field=models.ForeignKey(to='consolidacion.Etapa'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='invitado_por',
            field=models.ManyToManyField(to='consolidacion.Invitador', blank=True),
        ),
        migrations.AddField(
            model_name='creyente',
            name='ocupacion',
            field=models.ManyToManyField(to='consolidacion.Ocupacion', blank=True),
        ),
        migrations.AddField(
            model_name='creyente',
            name='sexo',
            field=models.ForeignKey(to='consolidacion.Sexo'),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='etapas',
            field=models.ManyToManyField(to='consolidacion.Etapa'),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='lider',
            field=models.ForeignKey(default=5, to='consolidacion.Lider'),
        ),
        migrations.AddField(
            model_name='consolidador',
            name='sexo',
            field=models.ForeignKey(to='consolidacion.Sexo'),
        ),
    ]
