# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0027_consolidador_etapas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consolidador',
            name='etapas',
            field=models.ManyToManyField(to='consolidacion.Etapa', blank=True),
        ),
    ]
