# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0052_auto_20160929_2046'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trayectoria',
            old_name='trayecto',
            new_name='completado',
        ),
        migrations.AddField(
            model_name='trayectoria',
            name='proceso',
            field=models.CharField(default='Nada', max_length=20, unique=True),
            preserve_default=False,
        ),
    ]
