# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0007_auto_20150804_0225'),
    ]

    operations = [
        migrations.RenameField(
            model_name='consolidador',
            old_name='consolidadondo',
            new_name='consolidando',
        ),
        migrations.AlterField(
            model_name='creyente',
            name='etapa',
            field=models.ForeignKey(blank=True, to='consolidacion.Etapa', null=True),
        ),
    ]
