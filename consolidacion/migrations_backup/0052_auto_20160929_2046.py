# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0051_auto_20160929_2042'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trayectoria',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('trayecto', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterField(
            model_name='creyente',
            name='trayecto',
            field=models.ForeignKey(null=True, blank=True, to='consolidacion.Trayectoria'),
        ),
        migrations.DeleteModel(
            name='Trayecto',
        ),
    ]
