# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0031_auto_20150819_1304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consolidador',
            name='eslider',
        ),
        migrations.AddField(
            model_name='consolidador',
            name='es_lider',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='consolidador',
            name='etapas',
            field=models.ManyToManyField(to='consolidacion.Etapa'),
        ),
    ]
