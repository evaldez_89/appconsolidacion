# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0061_auto_20161009_1438'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='creyente',
            options={},
        ),
        migrations.AlterField(
            model_name='creyente',
            name='prox_llamada',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='prox_visita',
            field=models.DateField(blank=True, null=True),
        ),
    ]
