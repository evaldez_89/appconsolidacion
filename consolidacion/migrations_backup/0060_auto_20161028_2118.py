# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0059_auto_20161028_2035'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='prox_llamada',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='prox_visita',
            field=models.DateField(blank=True, null=True),
        ),
    ]
