# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0045_auto_20160928_0021'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='sexo',
        ),
        migrations.AddField(
            model_name='creyente',
            name='sexo',
            field=models.ManyToManyField(to='consolidacion.Sexo'),
        ),
    ]
