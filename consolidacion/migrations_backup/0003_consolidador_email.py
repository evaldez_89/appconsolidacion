# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0002_auto_20150516_2002'),
    ]

    operations = [
        migrations.AddField(
            model_name='consolidador',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='e-mail', blank=True),
        ),
    ]
