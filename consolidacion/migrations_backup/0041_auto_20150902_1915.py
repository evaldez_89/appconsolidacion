# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0040_auto_20150902_1854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='creyente',
            name='fecha_culto',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='creyente',
            name='fecha_nacimiento',
            field=models.DateField(null=True, blank=True),
        ),
    ]
