# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0009_auto_20150810_1735'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='actividad',
            options={'verbose_name_plural': 'Actividades'},
        ),
    ]
