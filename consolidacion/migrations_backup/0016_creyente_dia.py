# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0015_auto_20150810_1920'),
    ]

    operations = [
        migrations.AddField(
            model_name='creyente',
            name='dia',
            field=models.ForeignKey(default=0, to='consolidacion.Dia'),
            preserve_default=False,
        ),
    ]
