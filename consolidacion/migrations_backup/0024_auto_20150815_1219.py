# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0023_auto_20150813_0940'),
    ]

    operations = [
        migrations.AddField(
            model_name='consolidador',
            name='foto',
            field=models.ImageField(blank=True, upload_to='consolidadores'),
        ),
        migrations.AddField(
            model_name='creyente',
            name='foto',
            field=models.ImageField(blank=True, upload_to='creyentes'),
        ),
        migrations.AddField(
            model_name='invitador',
            name='foto',
            field=models.ImageField(blank=True, upload_to='invitadores'),
        ),
        migrations.AddField(
            model_name='lider',
            name='foto',
            field=models.ImageField(blank=True, upload_to='lideres'),
        ),
    ]
