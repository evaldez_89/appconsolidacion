# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0053_auto_20160929_2055'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='creyente',
            name='trayecto',
        ),
        migrations.AddField(
            model_name='creyente',
            name='bautizado',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='creyente',
            name='encuentro',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='creyente',
            name='escuela_lideres',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='creyente',
            name='reencuentro',
            field=models.BooleanField(default=False),
        ),
    ]
