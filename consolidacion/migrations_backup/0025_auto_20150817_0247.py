# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('consolidacion', '0024_auto_20150815_1219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actividad',
            name='actividad',
            field=models.CharField(unique=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='civil',
            name='estado_civil',
            field=models.CharField(unique=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='dia',
            name='dia',
            field=models.CharField(unique=True, max_length=12),
        ),
        migrations.AlterField(
            model_name='etapa',
            name='etapa',
            field=models.CharField(unique=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='invitador',
            name='invitador',
            field=models.CharField(unique=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='laboral',
            name='estado_laboral',
            field=models.CharField(unique=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='ocupacion',
            name='ocupacion',
            field=models.CharField(unique=True, max_length=20),
        ),
        migrations.AlterField(
            model_name='sexo',
            name='sexo',
            field=models.CharField(unique=True, max_length=20),
        ),
    ]
