# -*- coding: utf-8 -*-
from datetime import datetime, date, timedelta
from django.template import loader
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from consolidacion import models
from decouple import config, Csv

from consolidacion.enumerables import Access


def get_hoy(add_days=0):
	today = date.today() + timedelta(days=add_days)
	return today.strftime("%d/%m/%Y")


def calc_edad(cumple, hoy):  # calcula la edad según la fecha
	nac = cumple
	hoy = hoy
	edad = hoy.year - nac.year

	try:  # revisar si el dia es 29 de feb y si el año es bisiesto
		hoy.replace(day=nac.day, month=nac.month)

		# si no ha llegado el mes devuelve el calculo original-1
		if hoy.month < nac.month:
			edad -= 1
		# si llego el mes
		elif hoy.month == nac.month:
			# pero no ha llegado el dia devuelve el calculo original
			if hoy.day < nac.day:
				edad -= 1
			# si llego o paso el dia, devuelve el calculo original + 1
			else:
				if hoy.day >= nac.day:
					edad = edad
		# si paso el mes devuelve el calculo original
		else:
			edad = edad

	except ValueError:  # si año no es bisiesto entonces:

		# si no ha llegado el mes devuelve el calculo original-1
		if hoy.month < nac.month:
			edad -= 1
		# si llego el mes
		elif hoy.month == nac.month:
			# pero no ha llegado el dia devuelve el calculo original
			if hoy.day < int(nac.day) - 1:
				edad -= 1
			# si llego o paso el dia, devuelve el calculo original + 1
			else:
				if hoy.day >= int(nac.day) - 1:
					edad = edad
		# si paso el mes devuelve el calculo original
		else:
			edad = edad
	return int(edad)


def calc_etapa(model, edad):  # calcula la etapa según la edad
	edad = int(edad)
	if edad <= 12:  # 12 o menos
		e = model.objects.get(etapa='Pre-Adolecente')

	elif 12 < edad <= 17:  # entre 13 y 17
		e = model.objects.get(etapa='Adolecente')

	elif 17 < edad <= 24:  # entre 18 y 24
		e = model.objects.get(etapa='Joven')

	elif 24 < edad <= 35:  # entre 25 y 35
		e = model.objects.get(etapa='Adulto Joven')

	elif 35 < edad <= 60:  # entre 36 y 60
		e = model.objects.get(etapa='Adulto')

	else:  # mas de 60
		e = model.objects.get(etapa='Vejez')
	return e


def calc_dia(model, fecha):  # numero del dia de la semana Lunes = 0 y Domingo = 6
	today = datetime.strptime(get_hoy(), '%d/%m/%Y')
	nday = 0

	if fecha is None:
		fecha_hoy = datetime.strftime(today, '%d/%m/%Y')
		nday = datetime.strptime(fecha_hoy, '%d/%m/%Y').weekday()

	else:
		fecha_culto = datetime.strftime(fecha, '%d/%m/%Y')
		nday = datetime.strptime(fecha_culto, '%d/%m/%Y').weekday()

	# print("El dia es: ", nday)
	d = model.objects.get(id=nday)

	return d


def enviaMail(categoria, consolidador=None, usuario=None, creyente=None):
	# Categoría 1 = creación de usuario
	# Categoría 2 = creyente asignado
	# Categoría 3 = creyente agregado
	# Categoría 4 = reenviar lista creyentes asignados
	# Categoría 5 = lista semanal
	# Categoría 6 = notificación

	link = config('HOST_ADDRESS')
	number = config('SERVICE_NUMBER')
	replay = config('EMAILS_TO_SEND_INFORMATION', cast=Csv())
	email_from = config('EMAIL_HOST_USER')
	msj = ""

	if consolidador is not None and len(consolidador.email) > 13:
		replay.insert(0, consolidador.email)

	if categoria == 1:
		sbj = usuario.first_name + " Usuario Creado"
		replay.insert(0, usuario.email)
		msj = loader.render_to_string('emails/welcome_message.html', {'link': link,
																	  'support': replay[-1],
																	  'numero': number,
																	  'usuario': usuario
																	  })
		# print(msj.encode('utf-8'))

	if categoria == 2:
		sbj = creyente.nombre + " le ha sido asignado"
		msj = loader.render_to_string('emails/mail_creyente_asignado.html',
									  {
										  'consolidador': consolidador.nombre,
										  'creyente': creyente.nombre,
										  'tel': creyente.telefono,
										  'cel': creyente.celular,
										  'otro': creyente.otro,
										  'direccion': creyente.direccion,
										  'link': link,
										  'id': str(creyente.id),
										  'num_consolidacion': number,
										  'support': replay[-1],
									  })

	if categoria == 3:
		sbj = creyente.nombre + " Agregado"
		msj = loader.render_to_string('emails/mail_creyente_creado.html',
									  {
										  'usuario': usuario,
										  'creyente': creyente.nombre,
										  'tel': creyente.telefono,
										  'cel': creyente.celular,
										  'otro': creyente.otro,
										  'link': link,
										  'id': str(creyente.id),
									  })

	if categoria == 4:
		sbj = "Listado Creyentes Asignados"

		# suma la lista anterior con una lista donde los creyentes tienen como consolidador
		# secundario al usuario en sesión
		creyente_list = models.Creyente.objects.filter(consolidador=consolidador.id).values_list('nombre', flat=True)

		msj = loader.render_to_string('emails/mail_enviar_listado.html', {
			'consolidador': consolidador.nombre,
			'creyentes': creyente_list,
			'link': link,
			'numero': number,
			'support': replay[-1],
		})

	if categoria == 5:
		sbj = "Listado Semanal"
		lista = list_last_added(7)
		lista_usuarios = [user.email for user in User.objects.filter(groups=6)]

		if len(lista) > 0:
			print('hubo conversiones')
			msj = loader.render_to_string('emails/weekly_mail.html',
										  {
											  'fecha': get_hoy(-7),
											  'lista': lista,
											  'link': link,
											  'support': replay[-1],
										  })
		else:
			msj = "No hubo conversiones"
		send_mail(sbj, msj, email_from,
				  lista_usuarios, fail_silently=False, html_message=msj)

	if categoria == 6:
		sbj = "Notificación Sobre Creyente"
		nota = creyente.nota.split('\n')[-2] + "\n" + creyente.nota.split('\n')[-1]
		msj = loader.render_to_string('emails/mail_notificacion.html',
									  {
										  'usuario': usuario,
										  'creyente': creyente,
										  'link': link,
										  'nota': nota
									  })

	if categoria == 7:
		sbj = "Notificación Creyente Consolidado"
		msj = loader.render_to_string('emails/mail_notificacion.html',
									  {
										  'usuario': usuario,
										  'creyente': creyente,
										  'link': link
									  })

	# si es un reenvío solo enviar al consolidador
	if categoria == 4:
		send_mail(sbj, msj, email_from,
				  [replay[0]], fail_silently=True, html_message=msj)
	else:
		if len(msj) > 0 and categoria != 5:
			send_mail(sbj, msj, email_from,
					  replay, fail_silently=True, html_message=msj)

	"""
	email = EmailMessage(
	sbj, str(msj),
	'iglesiadediosmetropolitanasdn@gmail.com',
	[consolidador.email],
	['enmanuel.mota@gmail.com', replay],
	reply_to=[replay], fail_silently=True, html_message=msj)"""
	# email.send()


def send_lider_network_lists(lider_id=None):
	sbj = "Creyentes Asignados A Su Red"
	email_from = config('EMAIL_HOST_USER')
	link = config('HOST_ADDRESS')
	support = config('EMAILS_TO_SEND_INFORMATION', cast=Csv())[-1]

	lideres = models.Consolidador.active_objects.filter(es_lider=True).exclude(email=None)
	if lider_id:
		lideres = lideres.filter(id=lider_id)

	for lider in lideres:
		consolidadores = models.Consolidador.active_objects.filter(leader_id=lider.id).values_list('id', flat=True)

		if consolidadores:
			creyente_list = models.Creyente.objects.by_lider(consolidadores)

			msj = loader.render_to_string('emails/mail_enviar_listado_red.html',
										{
											'lider': lider,
											'creyentes': creyente_list,
											'link': link,
											'support': support,
										})

			send_mail(sbj, msj, email_from, [lider.email], fail_silently=True, html_message=msj)


def crear_usuario(persona, staff=False):
	consolidador = Group.objects.get(name="consolidador")
	lider = Group.objects.get(name="lider")
	usuario = ""

	# comprobar si tiene correo y si es valido por tener "@"
	if len(persona.email) > 0 and persona.email.find("@") == 1:
		usuario = persona.email.rpartition("@")[0]
	# si no tiene, usar el nombre para crear el usuario
	else:
		nombre = persona.nombre.rsplit()
		if len(nombre) > 1:
			usuario = nombre[0] + nombre[1]

	# si el nombre de usuario se creo
	if len(usuario) > 0:
		usuario = usuario.lower().strip()
		# verificar que el nombre de usuario no exista
		# si no existe, crear usuario
		if not User.objects.filter(username=usuario).exists():

			# crear usuario
			user = User.objects.create_user(username=usuario,
											first_name=persona.nombre, password="1234")

			user.email = persona.email

			user.is_staff = staff
			if persona.es_lider:
				user.groups.add(lider)
			else:
				user.groups.add(consolidador)
			user.save()

			if persona.email:
				enviaMail(categoria=1, consolidador=persona, usuario=user)
				# enviaMail(persona, user, 1, None)

		else:
			print(str(usuario + " ya existe"))


def set_consolidador(nombre_creyente, nombre_consolidador):
	try:
		creyente = models.Creyente.objects.get(nombre=nombre_creyente)
		consolidador = models.Consolidador.active_objects.filter(nombre=nombre_consolidador).first()
		if consolidador is not None:
			creyente.consolidador = consolidador
			creyente.save()
		else:
			print(nombre_consolidador, " - Consolidador No Encontrado!")
	except Exception as error:
		print("Error al tratar de modificar creyente: ", nombre_creyente, ". ", error)


def multiple_add_creyente(creyente_info):



	approved_indexes: int = list()

	nombre = creyente_info[0].strip()

	# models.Creyente.objects.bulk_create()

	direccion = creyente_info[1].strip()

	edad = int(creyente_info[2].strip())

	tel = creyente_info[3].strip()

	fecha = creyente_info[4].strip()

	civil = creyente_info[5].strip()

	sexo = creyente_info[6].strip()

	try:
		fecha_culto = datetime.strptime(fecha, '%d/%m/%Y')

		sexo = models.Sexo.objects.get(sexo=sexo)

		id_actividad = 0
		if fecha_culto == datetime.strptime("05/05/2017", '%d/%m/%Y'):
			id_actividad = 9
		else:
			id_actividad = 2
		actividad = models.Actividad.objects.get(id=id_actividad)

		estado_civil = models.Civil.objects.get(estado_civil=civil)

		consolidador = None
		if len(creyente_info) > 7:
			consolidador = models.Consolidador.active_objects.get(nombre=creyente_info[7].strip())

		# nombre, dirección, edad, tel, fecha, civil, sexo, consolidador
		c, created = models.Creyente.objects.get_or_create(
			nombre=nombre,
			direccion=direccion,
			edad=edad,
			telefono=tel,
			estado_civil=estado_civil,
			fecha_culto=fecha_culto,
			sexo=sexo,
			actividad=actividad,
			consolidador=consolidador,
			nota="……Backup Data Loss…\nConfirmar Información"
		)

		if created:
			print(nombre, "created!")
		else:
			print("Already a creyente with that name: ", nombre)
	except Exception as error:
		print(error, creyente_info[0])


def crear_usuarios(nombre, email, esLider=False):
	consolidador = models.Consolidador.active_objects.get(nombre=nombre)

	grpConsolidador = Group.objects.get(name="consolidador")
	grpLider = Group.objects.get(name="lider")
	usuario = ""

	if len(email) > 0 and email.find("@") > 0:
		consolidador.email = email
		consolidador.save()
		usuario = email.rpartition("@")[0]

	# si el nombre de usuario se creo
	if len(usuario) > 0:
		usuario = usuario.lower().strip()
		# verificar que el nombre de usuario no exista
		# si no existe, crear usuario
		if not User.objects.filter(username=usuario).exists():

			# crear usuario
			user = User.objects.create_user(username=usuario,
											first_name=consolidador.nombre, password="1234")

			user.email = email

			if consolidador.es_lider:
				user.groups.add(grpLider)
			else:
				user.groups.add(grpConsolidador)
			user.save()
		else:
			print(str(usuario + " ya existe"))


def list_last_added(ctd):
	creyentes = []
	try:
		ctd = int(ctd)
		fromDate = datetime.today() + timedelta(days=-ctd)
		creyentes = models.Creyente.objects.filter(fecha_culto__gte=fromDate)
	except Exception as e:
		print(e)
	return creyentes


def users_report(sendmail=False):
	if sendmail:
		pass
	else:
		pass


def user_has_group(user, group_name: str):

	if type(user) == str:
		user = User.objects.filter(first_name=user).first()

	try:
		group = Group.objects.get(name=group_name)
		return group in user.groups.all()
	except ValueError:
		return False


def add_group(user_first_name: str, group_name: str) -> bool:
	successful = False
	user = User.objects.filter(first_name=user_first_name).first()
	if user is not None and not user_has_group(user, group_name):
		group = Group.objects.filter(name=group_name).first()

		if group is not None:
			user.groups.add(group)
			user.save()
			successful = True

	return successful


def access_level(user: User):
	if user.is_superuser:
		return Access.FULL
	if user.is_staff:
		return Access.STAFF
	if user_has_group(user, 'lider') and user_has_group(user, 'ministerio'):
		return Access.LIDER_MINISTERIO
	if user_has_group(user, 'ministerio'):
		return Access.MINISTERIO
	if user_has_group(user, 'lider'):
		return Access.LIDER
	if user_has_group(user, 'consolidador'):
		return Access.CONSOLIDADOR


def user_has_access(user: User, *levels: Access):
	try:
		required_access = sum(access.value for access in levels)
		if access_level(user).value >= required_access:
			return True
		else:
			return False
	except ValueError:
		return False
