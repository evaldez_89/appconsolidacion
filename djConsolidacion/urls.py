#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.urls import path
from django.views.generic import ListView

import consolidacion.views
import contacto.views
from consolidacion.forms import *

urlpatterns = [
    path('api/', include('rest_apis.urls')),
    path('telegram/', include('telegram_bot.urls')),
    url('^', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^export/xls/$', consolidacion.views.exportar_creyentes_xls, name='xls_report'),
    url(r'^import/xls/$', consolidacion.views.import_creyentes_xls, name='import_creyentes'),
    path(r'pwd_reset/', auth_views.PasswordResetView.as_view(
        template_name='registration/pwd_reset_form.html',
        html_email_template_name='registration/pwd_reset_email.html',
        email_template_name='registration/pwd_reset_email.html',
        subject_template_name='registration/pwd_reset_subject.txt'),
         name='password_reset'),
    path(r'pwd_reset/done/', auth_views.PasswordResetDoneView.as_view(
        template_name='registration/pwd_reset_done.html'), name='password_reset_done'),
    path(r'pwd_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
         auth_views.PasswordResetConfirmView.as_view(template_name='registration/pwd_reset_confirm.html'),
         name='password_reset_confirm'),
    path(r'pwd_reset/complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='registration/pwd_reset_complete.html'), name='password_reset_complete'),

    path(r'', view=auth_views.LoginView.as_view(template_name='registration/landing.html'), name='login'),
    path(r'inicio/', view=auth_views.LoginView.as_view(template_name='registration/landing.html'), name='login'),
    path(r'login/',   view=auth_views.LoginView.as_view(template_name='registration/landing.html'), name='login'),
    path(r'logout/', view=auth_views.LogoutView.as_view(next_page='inicio/'), name='logout'),

    url(r'^buscar_creyente/$', consolidacion.views.buscar, {'model': models.Creyente}, name='buscar_creyente'),
    url(r'^agrupar_creyente/(?P<group_by>[-\w]+)$', consolidacion.views.agruparCreyentes, name='group_by'),
    url(r'^buscar_consolidador/$', consolidacion.views.buscar, {'model': models.Consolidador},
        name='buscar_consolidador'),

    url(r'^contacto/$', contacto.views.contacto),
    url(r'^contacto/gracias/$', contacto.views.gracias),
    url(r'^consolidacion/creyente_agregado/$', consolidacion.views.agregado),

    url(r'^add_consolidador/$', consolidacion.views.add_consolidador, name='add_consolidador'),
    url(r'^add_celula/$', consolidacion.views.add_celula, name='add_celula'),
    url(r'^add_creyente/$', consolidacion.views.add_creyente, name='add_creyente'),
    url(r'^add_no_creyente/(?P<pk>[0-999]+)/$', consolidacion.views.add_no_creyente, 'add_no_creyente'),
    url(r'^add_ocupacion/$', login_required(AddOcupacion.as_view(), login_url='/login'), name='agregar-ocupacion'),
    url(r'^add_actividad/$', login_required(AddActividad.as_view(), login_url='/login'), name='agregar-actividad'),

    url(r'^creyentes/lista_consolidado$', consolidacion.views.lista_creyentes, {'tipo': 'consolidado'}),
    url(r'^ver_consolidadores/$', consolidacion.views.listaConsolidadores, name='ver_consolidadores'),
    url(r'^ver_celulas/$', consolidacion.views.listaCelula, name='ver_celulas'),
    url(r'^creyentes/lista_debaja$', consolidacion.views.lista_creyentes, {'tipo': 'baja'}, name='creyentes_de_baja'),
    url(r'^ver_creyentes/$', consolidacion.views.lista_creyentes, {'tipo': 'siguiendo'}, name='ver_creyentes'),
    url(r'^ver_lideres$', consolidacion.views.listalideres, name='ver_lideres'),

    url(r'^detalles_creyente/$', consolidacion.views.Plantilla,
        {'model': models.Creyente, 'template': 'plantilla_creyente.html'}, name="detalles_creyente"),
    url(r'^detalles_celula/$', consolidacion.views.Plantilla,
        {'model': models.Celula, 'template': 'celula_creyente_list.html'},),
    path(r'detalles_consolidador/', view=consolidacion.views.Plantilla,
         kwargs={'model': models.Consolidador, 'template': 'plantilla_consolidador.html'},
         name='detalle-consolidador'),
    url(r'^consolidador/(?P<pk>\d+)/crear_usuario/$', consolidacion.views.crear_usuario, name='crear-usuario'),
    url(r'^consolidador/(?P<pk>\d+)/add_group/$', consolidacion.views.add_group_to_user, name='add-group'),
    url(r'^consolidador/(?P<pk>\d+)/delete/$', consolidacion.views.delete_consolidador, name='delete-consolidador'),
    url(r'^consolidador/(?P<pk>\d+)/deactivate/$', consolidacion.views.deactivate_consolidador, name='deactivate-consolidador'),
    url(r'^consolidador/(?P<pk>\d+)/enviar_listado/$', contacto.views.enviar_listado,
        name='enviar_listado_consolidador'),
    url(r'^lider/(?P<pk>\d+)/enviar_listado/$', contacto.views.enviar_listado_lider, name='enviar_listado_lider'),

    url(r'^disp_celulas/$', consolidacion.views.showCelulas),
    url(r'^disp_celulas/(\d{1,5})/$', consolidacion.views.setCelula),
    url(r'^asignar_consolidador/(?P<pk>[0-99]+)$', consolidacion.views.show_consolidadores,
        name='consolidadores-disponibles'),
    url(r'^asignar_lider/(?P<pk>[0-99]+)$', consolidacion.views.show_lideres, name='lideres-disponibles'),
    url(r'^disp_consolidador/(?P<pk>[0-99]+)$', consolidacion.views.set_consolidador, name='asignar-consolidador'),
    url(r'^disp_consolidador/(?P<pk>[0-99]+)/(?P<consolidador_id>[0-99]+)$', consolidacion.views.set_consolidador, name='asignar-consolidador'),
    url(r'^disp_lideres/(\d{1,5})/$', consolidacion.views.set_lider, name='asignar-lider'),
    # url(r'^enviar_email/$', consolidacion.views.enviaMail),
    url(r'^dar_baja/creyente/(?P<pk>\d{1,5})/$', consolidacion.views.baja, name='dar-de-baja'),
    url(r'^dar_baja/creyente/debaja/(\d{1,5})/$', consolidacion.views.de_baja),

    url(r'^consolidado/creyente/(?P<pk>\d{1,5})$', consolidacion.views.consolidado, name='fue-consolidado'),
    url(r'^consolidado/creyente/enviado/(\d{1,5})/$', consolidacion.views.creyente_consolidado),

    url(r'^creyente/(?P<pk>[0-99]+)/nota/$',    consolidacion.views.agregarNota, name='add-note'),
    url(r'^celula/(?P<pk>[0-99]+)/reporte/$',   contacto.views.enviar_reporte, name='send-report'),

    url(r'^consolidador/(?P<pk>[0-99]+)/editar/$',  login_required(ActualizarConsolidador.as_view(),
                                                                   login_url='/login'),
        name='actualizar-consolidador'),
    url(r'^creyente/(?P<pk>[0-99]+)/editar/$',      login_required(ActualizarCreyente.as_view(), login_url='/login'),
        name='actualizar-creyente'),
    url(r'^ocupacion/(?P<pk>[0-99]+)/editar/$',     login_required(ActualizarOcupacion.as_view(), login_url='/login'),
        name='actualizar-ocupacion'),
    url(r'^actividad/(?P<pk>[0-99]+)/editar/$',     login_required(ActualizarActividad.as_view(), login_url='/login'),
        name='actualizar-actividad'),

    url(r'^promover_consolidador/(?P<nombre>[\w|\W]+)/$', consolidacion.views.promover_consolidador,
        name='promover_consolidador'),
    url(r'^promover_creyente/(?P<nombre>[\w|\W]+)/$', consolidacion.views.promover_creyente, name='promover_creyente'),


    url(r'^creyentes_disponibles/$', consolidacion.views.available_creyentes, name='disponibilidad_creyentes'),


    url(r'^disponibilidad_creyentes/(?P<pk>[0-99]+)$', consolidacion.views.available_creyentes,
        name='disponibilidad_creyentes'),
    url(r'^asignar_los_creyentes/(?P<pk>[0-99]+)/$', consolidacion.views.set_creyentes, name='asignar_creyentes'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)