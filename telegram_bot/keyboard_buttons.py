from telegram import KeyboardButton, InlineKeyboardButton


class Buttons:
    Start = KeyboardButton("Iniciar", request_contact=True)
    SaveLocation = KeyboardButton("Guardar Ubicación")
    __showCreyentes: InlineKeyboardButton
    __showConsolidador: InlineKeyboardButton
    Exit = KeyboardButton("Salir")

    def get_buttons_to_show_info(self, consolidador_id: int = None, leader_id: int = None):
        buttons = []
        if consolidador_id:
            self.ShowCreyentes = consolidador_id
            buttons.append(self.ShowCreyentes)
        if leader_id:
            self.ShowConsolidadores = leader_id
            buttons.append(self.ShowConsolidadores)

        return buttons

    @property
    def ShowCreyentes(self):
        return self.__showCreyentes

    @ShowCreyentes.setter
    def ShowCreyentes(self, consolidador_id: int):
        self.__showCreyentes = InlineKeyboardButton('Ver Creyentes', callback_data=f'consolidador_id:{consolidador_id}')

    @property
    def ShowConsolidadores(self):
        return self.__showConsolidador

    @ShowConsolidadores.setter
    def ShowConsolidadores(self, leader_id: int):
        self.__showConsolidador = InlineKeyboardButton('Ver Consolidadores', callback_data=f'lider_id:{leader_id}')

    def get_button_by_name(self, button_name: str) -> KeyboardButton:
        selected_button = []
        if button_name:
            buttons = [getattr(self, prop) for prop in dir(self) if type(getattr(self, prop)) == KeyboardButton]
            selected_button = [button for button in buttons if getattr(button, 'text') == button_name]

        return selected_button[0] if selected_button else None
