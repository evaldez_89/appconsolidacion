from django.urls import path

from . import views

urlpatterns = [
    path('webhook/', views.event, name='webhook'),
]
