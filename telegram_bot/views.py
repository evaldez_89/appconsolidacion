import json
import re
from collections import defaultdict
from os import linesep

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup, ReplyMarkup
from telegram.bot import Bot
from telegram.parsemode import ParseMode

from consolidacion.models import Consolidador, Creyente
from consolidacion.templatetags.etiquetas import totalCreyentes
from djConsolidacion.settings import TELEGRAM_BOT_TOKEN
from .keyboard_buttons import Buttons

IDM_BOT = Bot(TELEGRAM_BOT_TOKEN)
BUTTONS = Buttons()


def get_consolidador_buttons(consolidador_id: int, name: str, creyentes_total: int) -> [InlineKeyboardButton]:
    return [
        InlineKeyboardButton(text=f"{name} - ({creyentes_total})", callback_data=f'consolidador_id:{consolidador_id}')
    ]


def get_keyboard(buttons: [[InlineKeyboardButton]] = None) -> ReplyMarkup:
    if buttons:
        return InlineKeyboardMarkup(buttons)
    else:
        return ReplyKeyboardMarkup([[BUTTONS.Start]], resize_keyboard=True, one_time_keyboard=True)


def get_consolidadores_data(lider_id: int):
    return Consolidador.active_objects.filter(leader_id=lider_id).values_list('id', 'nombre')


def get_requested_data(**kwargs):
    response_data = {
        'buttons': [],
        'creyentes_info': defaultdict(list),
        'as_leader_id': False,
        'message': False,
        'error_message': False
    }

    phone_number = kwargs.get('phone_number')
    consolidador_id = kwargs.get('consolidador_id')
    lider_id = kwargs.get('lider_id')

    if phone_number:
        conso = Consolidador.objects.first()
        print(conso.nombre)
        consolidador = Consolidador.active_objects.find_by_phone_number(phone_number)
        if len(consolidador) == 0:
            response_data.update({"error_message": "Su número no esta configurado en el sistema"})
        elif len(consolidador) > 1:
            response_data.update({"error_message": "Su número está mal configurado en el sistema"})
        else:
            consolidador = consolidador.first()
            response_data.get('buttons').append(
                BUTTONS.get_buttons_to_show_info(consolidador.id, consolidador.as_leader_id())
            )

        response_data.update({'message': 'Elija una opción'})
    elif consolidador_id:
        consolidador = Consolidador.active_objects.filter(id=consolidador_id).first()
        creyentes = Creyente.objects.by_consolidador(consolidador.id)

        for creyente in creyentes:
            response_data.get('creyentes_info')[creyente.consolidador.nombre].append(
                f"<b>{creyente.nombre}</b>: {creyente.contact_info.strip()}"
            )

        for consolidador_name, creyente_info in response_data.get('creyentes_info').items():
            message = response_data.get('message') or ""
            subtitle = f"<b>Consolidador</b>: {consolidador_name}\n"
            message += f"{subtitle}{f'{linesep}'.join(creyente_info)}{linesep}"
            response_data.update({'message': message})
    elif lider_id:
        response_data.update({'message': 'Listado de Consolidadores'})
        consolidadores = get_consolidadores_data(lider_id)
        for consolidador in consolidadores:
            response_data.get('buttons').append(
                get_consolidador_buttons(consolidador[0], consolidador[1], totalCreyentes(consolidador[0]))
            )
        if not consolidadores:
            response_data.update({'error_message': 'No tiene consolidadores asignados'})

    return response_data


def parse_response_arguments(request_body: dict) -> dict:
    callback_query = request_body.get('callback_query', {})
    callback_data = callback_query.get('data')

    kwargs = {value[0]: value[1] for value in [callback_data.split(':')]} if callback_data else {}

    message = request_body.get('message') if not callback_query else callback_query.get('message')
    contact = message.get('contact')
    phone_number = None

    if contact:
        phone_number = contact.get('phone_number')

        for regex in (r'8[0|2|4]9\d{3}\d{4}',):  # TODO: Set list on .env file or improve regex to match more cases
            match = re.search(regex, phone_number).group()
            if match:
                phone_number = match
                break

    response = get_requested_data(phone_number=phone_number, **kwargs)
    error_message = response.get('error_message', f"No tengo una respuesta "
                                                  f"para el mensaje '{message.get('text', '')}'")
    text = response.get('message', error_message) or 'Selecciona una opción'

    return {
        'chat_id': message.get('chat', {'id': 0}).get('id') if message else 0,
        'text': text,
        'parse_mode': ParseMode.HTML,
        'buttons': response.get('buttons')
    }


@csrf_exempt
def event(request):
    body = json.loads(request.body)

    response = parse_response_arguments(body)

    IDM_BOT.send_message(
            chat_id=response.get('chat_id'),
            text=response.get('text'),
            parse_mode=response.get('parse_mode'),
            reply_markup=get_keyboard(response.get('buttons'))
        )

    return JsonResponse({'status': 'ok', 'message': 'worked'})
