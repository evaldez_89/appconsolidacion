##Antes de instalar los requerimientos de python
###Linux (Ubuntu)
`sudo apt-get install mysql-server`

`sudo apt-get install libmysqlclient-dev`

`sudo apt-get install libmariadbclient-dev`

# Pendientes:
- Mostrar el campo edad, si no hay fecha de nacimiento calcular la etapa en base a esta
- Validar que si no hay fecha de nacimiento debe haber edad o si no hay edad, debe haber
fecha de nacimiento.
    - También pudiera hacer que el campo fecha sea genérico y que
si la longitud es 2, que lo identifique como la edad y si es mas que trate de 
convertirlo a fecha, si es valida, calcular la edad

- Botón editar al lado de los status de los creyentes, debe de abrir una ventana emergente donde
presente un formulario con 2 campos, los cuales cargaran las fechas de visita/llamada que tiene
el creyente para actualizarlas a una fecha futura. 
  > El botón esta, falta la función

- En la lista de creyentes, al lado del status, habilitar un botón el 
cual al pulsarlo le envíe un correo al consolidador encargado para que recuerde darle el 
seguimiento, llamada o visita.
  > El botón esta, falta la función, puede ser un filtro

- Mandar correos automáticamente a consolidadores, dependiendo en la fecha de visita o llamada
del creyente.
  - Debería de programar una acción a cierta hora todos los días, que recoja toda
la información, y mande los correos.
    > ¿Como programo una acción automática?

