from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView

from consolidacion import models
from .serializers import *


class CreyenteShortViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Creyente.objects.order_by('-fecha_creacion').all()
    serializer_class = CreyenteShortSerializer

    def get_queryset(self):
        results = super(CreyenteShortViewSet, self).get_queryset()

        for key, value in self.request.GET.items():
            results = results.filter(**{key: value})

        return results


class CreyenteViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Creyente.objects.order_by('fecha_creacion').all()
    serializer_class = CreyenteSerializer

    def get_queryset(self):
        results = super(CreyenteViewSet, self).get_queryset()

        for key, value in self.request.GET.items():
            results = results.filter(**{key: value})

        return results


class ConsolidadorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Consolidador.active_objects.all()
    serializer_class = ConsolidadorSerializer

    def get_queryset(self):
        results = super(ConsolidadorViewSet, self).get_queryset()

        for key, value in self.request.GET.items():
            results = results.filter(**{key: value})

        return results


class ConsolidadorShortViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = models.Consolidador.active_objects.all()
    serializer_class = ConsolidadorShortSerializer

    def get_queryset(self):
        results = super(ConsolidadorShortViewSet, self).get_queryset()

        for key, value in self.request.GET.items():
            results = results.filter(**{key: value})

        return results


class CustomTokenObtainPairView(TokenObtainPairView):
    def get_serializer(self, *args, **kwargs):
        data = kwargs.get('data')
        if data:
            email = data.get('username')
            user = User.objects.filter(email=email).first()
            username = user.username if user else email
            data.update({'username': username})
        return super(CustomTokenObtainPairView, self).get_serializer(*args, **kwargs)

    serializer_class = CustomTokenObtainPairSerializer


class ObjectCountView(APIView):
    renderer_classes = (JSONRenderer, )

    model_querysets = {
        'creyente': Creyente,
        'consolidador': Consolidador,
    }

    def get(self, request, model, parent_model=None, parent_id=None, format=None):
        query = self.model_querysets.get(model)
        count = 0
        if parent_model and parent_id and query:
            if parent_model == 'lider' and model == 'creyente':
                leader = models.Consolidador.active_objects.get(id=parent_id)
                query = getattr(query.objects, 'lider_network')
                count = query(leader).count()
            else:
                filter_keyword = {f"{parent_model}_id": parent_id}
                count = query.objects.filter(**filter_keyword).count()
        elif query:
            count = query.objects.count()

        content = {f'count': count}
        return Response(content)
