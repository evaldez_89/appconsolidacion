from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

from . import views

v1_router = routers.DefaultRouter()
v1_router.register('creyentes', views.CreyenteViewSet)
v1_router.register('consolidadores', views.ConsolidadorViewSet)
v1_router.register('consolidadores-short-details', views.ConsolidadorShortViewSet)
v1_router.register('creyentes-short-details', views.CreyenteShortViewSet)

urlpatterns = [
    path('token/', views.CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('v1/count/<str:model>/', views.ObjectCountView.as_view(), name='object-count'),
    path('v1/count/<str:model>/<str:parent_model>/<int:parent_id>', views.ObjectCountView.as_view(),
         name='object-count-filtered'),
    path('v1/', include(v1_router.urls)),
]