from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from consolidacion.models import Creyente, Consolidador
from rest_framework import serializers


class ConsolidadorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consolidador
        fields = '__all__'


class ConsolidadorShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consolidador
        fields = ['id', 'nombre', 'url']


class CreyenteShortSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Creyente
        fields = ['id', 'nombre', 'telefono', 'url', 'consolidador']


class CreyenteSerializer(serializers.HyperlinkedModelSerializer):
    etapa = serializers.ReadOnlyField(source='etapa.etapa')
    estado_civil = serializers.ReadOnlyField(source='civil.estado_civil')
    sexo = serializers.ReadOnlyField(source='sexo.sexo')
    actividad = serializers.ReadOnlyField(source='actividad.actividad')
    estado_laboral = serializers.ReadOnlyField(source='estado_laboral.estado_laboral')
    ocupacion = serializers.ReadOnlyField(source='ocupacion.ocupacion')
    dia = serializers.ReadOnlyField(source='dia.dia')
    razon = serializers.ReadOnlyField(source='razon.razon')
    consolidador = ConsolidadorShortSerializer()
    invitado_por = serializers.RelatedField(source='invitado_por.nombre', read_only=True)

    class Meta:
        model = Creyente
        fields = '__all__'


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        token = super(CustomTokenObtainPairSerializer, self).validate(attrs)
        consolidador_id = Consolidador.active_objects.filter(email=self.user.email).first()
        lider = {
            'id': consolidador_id.leader.id,
            'name': consolidador_id.leader.nombre,
        }
        data = {
            'username': self.user.username,
            'first_name': self.user.first_name,
            'last_name': self.user.last_name,
            'consolidador_id': consolidador_id.id,
            'is_leader': consolidador_id.es_lider,
            'as_leader_id': consolidador_id.id if consolidador_id.es_lider else None,
            'gender': consolidador_id.sexo.sexo,
            'email': self.user.email,
            'token': token,
            'leader': lider
        }

        return data
