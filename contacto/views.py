from django.contrib.auth.decorators import login_required

from Funciones import enviaMail, get_hoy, send_lider_network_lists
from consolidacion import models
from contacto.forms import FormularioContactos
from django.core.mail import send_mail
from django.http.response import HttpResponseRedirect
from django.shortcuts import render


def contacto(request):
    if request.method == 'POST':
        form = FormularioContactos(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            asunto = 'Llamar a ' + cd['nombre']
            mensaje = "Hola mi nombre es " + cd['nombre'] + '\n\n' + \
                      cd['mensaje'] + '\n\n' + cd['telefono'] + '\n' + \
                      cd['direccion'] + '\n' + cd['email']

            send_mail(asunto,
                      mensaje,
                      cd.get('email', 'noreply@example.com'),
                      ['siteowner@example.com'], )

            return HttpResponseRedirect('/contacto/gracias/')
    else:
        form = FormularioContactos(initial={'mensaje': 'Bendiciones'})
    return render(request, 'form_contacto.html', {'form': form})


def gracias(request):
    return render(request, 'form_gracias.html')


@login_required(login_url='/login')
def enviar_listado(request, pk):
    consolidador = models.Consolidador.active_objects.get(id=int(pk))
    if consolidador.email:
        enviaMail(categoria=4, consolidador=consolidador)
        return HttpResponseRedirect('/detalles_consolidador/?id=' + pk)
    else:
        return HttpResponseRedirect('/consolidador/' + pk + '/editar')


@login_required(login_url='/login')
def enviar_reporte(request, pk):
    celula = models.Celula.objects.get(id=int(pk))
    creyentes = []
    for creyente_id in request.POST.getlist('creyentes'):
        creyente = models.Creyente.objects.get(id=int(creyente_id))

        if 'bautizado' + creyente_id in request.POST:
            creyente.bautizado = request.POST['bautizado' + creyente_id]
        if 'encuentro' + creyente_id in request.POST:
            creyente.encuentro = request.POST['encuentro' + creyente_id]
        if 'reencuentro' + creyente_id in request.POST:
            creyente.reencuentro = request.POST['reencuentro' + creyente_id]
        if 'escuela_lideres' + creyente_id in request.POST:
            creyente.escuela_lideres = request.POST['escuela_lideres' + creyente_id]
        if 'nota' + creyente_id in request.POST:
            nota = request.POST['nota' + creyente_id].replace("\n", " ").replace("\r", " ")
            creyente.nota += '\n' + request.user.first_name + ' ' + get_hoy() + ":\n" + nota + '\r\n'

        creyente.save()

        creyentes.append(creyente)

    if 'nota_general' in request.POST:
        nota = request.POST['nota_general'].replace("\n", " ").replace("\r", " ")
        celula.nota += '\n' + request.user.first_name + ' ' + get_hoy() + ":\n" + nota + '\r\n'

        celula.save()

        # TODO: enviar el reporte por email

    return HttpResponseRedirect("/detalles_creyente/?id=")


@login_required(login_url='/login')
def enviar_listado_lider(request, pk):
    if pk:
        send_lider_network_lists(pk)
        return HttpResponseRedirect('/ver_lideres')
    else:
        return HttpResponseRedirect('/lider/' + pk + '/editar')