#!/usr/bin/env python
# -*- coding: utf-8 -*-from django import forms
from django import forms


class FormularioContactos(forms.Form):
    nombre = forms.CharField(max_length=20)
    telefono = forms.CharField(max_length=15, min_length=10)
    direccion = forms.CharField(required=False, max_length=40)
    email = forms.EmailField(required=False, max_length=40)
    mensaje = forms.CharField(widget=forms.Textarea)