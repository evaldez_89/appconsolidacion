from datetime import date

from django.core.management.base import BaseCommand, CommandError

from contacto.management.commands._utilities import send_lider_network


class Command(BaseCommand):
    help = "Enviar un correo mensual a cada líder con el listado de creyentes asignados a su red."

    def add_arguments(self, parser):
        parser.add_argument('--day',
                            help='Day of the month to execute the action.',
                            type=int)

    def handle(self, *args, **options):
        sent = False
        comment = "Acción de enviar correo a cada líder ejecutada"
        day_of_month = options['day']

        process_date = date.today()
        
        if not day_of_month:
            day_of_month = process_date.day
        
        self.stdout.write(f"Envio de listado de creyentes por red {process_date}")
        try:
            self.stdout.write(f"El dia configurado para ejecutar es: {day_of_month}")
            self.stdout.write(f"Hoy es el día número {process_date.day} del mes")
            if process_date.day == day_of_month:
                self.stdout.write(f"Se tratará de enviar un correo a cada líder en fecha: {process_date}")
                send_lider_network()
                sent = True
        except Exception:
            comment = f"{process_date} - Hubo un error al tratar de enviar el correo mensual a cada líder."
            raise CommandError(comment)
        
        if sent:
            comment += " - Correo Enviado"
        self.stdout.write(comment)
