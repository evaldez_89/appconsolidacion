from datetime import date

from django.core.management.base import BaseCommand, CommandError

from contacto.management.commands._utilities import send_weekly_mail


class Command(BaseCommand):
    help = "Enviar un correo semanal con el listado de las personas que se convirtieron"

    def add_arguments(self, parser):
        parser.add_argument('--day',
                            help='Day of the week to execute the action.',
                            type=int)

    def handle(self, *args, **options):
        process_date = date.today()
        day_of_week = options['day'] if options['day'] else 1

        sent = False
        self.stdout.write(f"Se tratará de enviar el correo en fecha: {process_date}")
        try:
            self.stdout.write(f"Hoy es: {process_date} es el día número {process_date.weekday()} de la semana")
            if process_date.weekday() == day_of_week:
                send_weekly_mail()
                sent = True
        except Exception as e:
            self.stdout.write(self.style.ERROR(e))
            raise CommandError(f"{process_date} - Hubo un error al tratar de enviar el correo semanal.")
        comment = "Acción de enviar correo ejecutada"
        if sent:
            comment += " - Correo Enviado"
        self.stdout.write(f"{process_date} - {comment}")
